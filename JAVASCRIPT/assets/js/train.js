<script>
        let tabPic = [];
        let tabPseudo = [];
        let tabNamePic =[];
        let tabUrlPic = [];                       
        let textPseudo = document.getElementById('pseudoForm');
        let textUrlAvatar = document.getElementById('urlAvatarForm');
        let textSubmitBtn = document.getElementById('submitBtn');
        let nextButton = document.getElementById('next');
        let previousButton = document.getElementById('previous');
        let delButton = document.getElementById('deleteButton');
        let table = document.getElementById('table');
        let picUser = document.getElementById('picUser');
        let addPic = document.getElementById('addPic');
        let namePic = document.getElementById('picName');
        let removePic = document.getElementById('removePic');

        let pseudoPic = document.getElementById('pseudo');
        let picturePic = document.getElementById('picture');
        let pictureContainer = document.getElementById('pictureContainer');

        let positionPic;
      

///////////////////////////////// EVENEMENT SUR LE BOUTON ENVOYER HEADER
        textSubmitBtn.addEventListener('click', e=>{  
            event.preventDefault();           
            let include = tabPseudo.includes(textPseudo.value);  
            // Si la valeur est différente et que le pseudo n'existe pas
            if(textPseudo.value != "" && !include)
            {                          
                //rajoute le pseudo et la src de l'image dans les tableaux
                tabPseudo.push(textPseudo.value);         
                tabPic.push(textUrlAvatar.value); 
                // positionPic prends la valeur de la taille du tableau - 1 
                // if(tabPseudo.length > 0){
                positionPic = tabPseudo.length-1;
                // }
                // else{
                //     positionPic = 0;
                // };
                pseudoPic.innerText=tabPseudo[positionPic];
                picturePic.setAttribute('src',tabPic[positionPic]);

//////////////////////////////////////////////////////////////////////////////////////

                // Creation des elements a rajouter dans la table
                let tr = document.createElement('tr');
                let td = document.createElement('td');
                let tbody = document.querySelector('tbody');
                td.innerText = textPseudo.value;
                tr.appendChild(td); 
                tbody.appendChild(tr);
                // Retirer le hidden de la table et supprimer le contenu du INP pseudo + sauvegarder en JSON
                table.classList.remove('hidden');
                pseudoForm.value = "";               
                // Sauvegarde
                localStorage.removeItem('picList')
                localStorage.removeItem('pseudoList')
                saveToJson();       
            };
            checkEmpty();

            // Si la liste contient le pseudo, affiche un toastr
            if(include)
            {
                toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }
                toastr["warning"]("Ce pseudo existe déjà!", "ERREUR")
           
            };
        })

// EVENEMENT NEXT
        nextButton.addEventListener('click', e=>{
            if(tabPic.length > 0)
            {
                if(positionPic >= tabPseudo.length-1)
                {
                    positionPic = 0;
                    pseudoPic.innerText=tabPseudo[positionPic];
                    picture.setAttribute('src',tabPic[positionPic]);
                }
                else
                {
                    positionPic++;
                    pseudoPic.innerText=tabPseudo[positionPic];
                    picture.setAttribute('src',tabPic[positionPic]);               
                }
            }          
        });

// EVENEMENT PREVIOUS
        previousButton.addEventListener('click', e=>{
            if(tabPic.length > 0)
            {
                if(positionPic <= 0)
                {
                    positionPic = tabPseudo.length-1;
                    pseudoPic.innerText=tabPseudo[positionPic];
                    picturePic.setAttribute('src',tabPic[positionPic]);              
                }
                else
                {
                    positionPic--;            
                    pseudoPic.innerText=tabPseudo[positionPic];
                    picturePic.setAttribute('src',tabPic[positionPic]);                   
                }
            }
        });



//EVENEMENT BUTTON DELETE HEADER
        delButton.addEventListener('click', e=>{
            event.preventDefault();
            // on ne supprime rien s'il n'y a pas de valeur dans le champs  
            
            if(textPseudo.value != "")
            {
                // recherche de l'index qui correspond à la valeur demandé et si une valeur est renvoyé, on supprime
                // sinon on reçoit un -1 et on ne rentre pas dans la boucle
                let indexDel = tabPseudo.indexOf(textPseudo.value);
                if(indexDel != -1)
                {            
                    tabPseudo.splice(indexDel,1);
                    tabPic.splice(indexDel,1);                                 
                    if(positionPic > 0)
                    {
                        positionPic--;
                    }    

                    picturePic.setAttribute('src',tabPic[positionPic]);
                    pseudoPic.innerText=tabPseudo[positionPic];

                    //Suppression de la clé dans la table
                    let tbody = document.querySelectorAll('tbody tr td');
                    for (let key of tbody) {

                        if (key.innerText == textPseudo.value)
                        {
                            key.parentNode.remove(key);
                        };

                    };
                    localStorage.removeItem('picList')
                    localStorage.removeItem('pseudoList')
                    saveToJson();  

                    // Si le tableau d'image est vide, on remet le src de l'image à null et le texte pseudo aussi
                    // l'index à 0 et on masque la table
                    if(tabPic.length == 0)
                    {
                    picturePic.setAttribute('src',"");
                    pseudoPic.innerText=""
                    index = 0;
                    positionPic = 0;  
                    table.classList.add('hidden');
                    };
                   
                }                                            
            };     
            checkEmpty();                                        
        });

//////////////////////////////////////////////////////////////////////////////////////
 // EVENEMENT AJOUTER UNE IMAGE FOOTER
        addPic.addEventListener('click',e=>{
            let option = document.createElement('option');                     
            option.value = picUser.value;
            option.innerText = namePic.value;
            textUrlAvatar.appendChild(option);
            picUser.value = "";
            namePic.value = "";          
        });




//EVENEMENT ENLEVER IMAGE FOOTER
        removePic.addEventListener('click',e=>{
            let len = textUrlAvatar.options.length;        
            if(len > 1){ 
                for (let i = 0; i< len; i++) {
                    if(textUrlAvatar.options[i].innerText == namePic.value){
                        textUrlAvatar.options[i] = null;
                        saveUrlToJson();                 
                    }               
                };                          
            }
            else{
                toastr["warning"]("il doit rester au moins une image", "ERREUR");
            }
        });


///////////////////////////// EVENEMENT AU CHARGEMENT DE LA PAGE
        document.addEventListener("DOMContentLoaded", e=>{
            document.getElementById("form").reset();
           // Recupere la sauvegarde dans les tableaux
           tabPic = JSON.parse(localStorage.getItem('picList')) || [];
           tabPseudo = JSON.parse(localStorage.getItem('pseudoList')) || [];
           // Si les tableaux ne sont pas vide position pic affichera la dernière image sinon rien
           if(tabPic.length > 0){
                positionPic = tabPic.length-1;
                pseudoPic.innerText=tabPseudo[positionPic];
                picturePic.setAttribute('src',tabPic[positionPic]);

                // Creation de la table et affichage de celle ci
                for (let i of tabPseudo) {
                    let tr = document.createElement('tr');
                    let td = document.createElement('td');
                    let tbody = document.querySelector('tbody');               
                    td.innerText = i;
                    tr.appendChild(td); 
                    tbody.appendChild(tr);
                    table.classList.remove('hidden');   
                }    
            }
            else
            {
                positionPic = 0;
                pseudoPic.innerText="";
                picturePic.setAttribute('src',"");
            };   
            checkEmpty();

///////////////////////////////////////////////////////////////////////////////
            
            // restauration de la sauvegarde des nouvelles images et de leurs noms
            // tabUrlPic = JSON.parse(localStorage.getItem('picUrlList')) || [];
            // tabNamePic = JSON.parse(localStorage.getItem('picNameList')) || [];
            // for (let i of tabUrlPic) {       
            //     let option = document.createElement('option');
            //     option.value = i;            
            //     option.innerText(tabNamePic[i]);                                      
            //     textUrlAvatar.appendChild(option);           
            // }
///////////////////////////////////////////////////////////////////////////////              
           checkEmpty();           
        });







// FONCTIONS ---------------------------------------------------
        function checkEmpty(){
            if(tabPic.length <= 1)
            {
                previousButton.classList.add('hidden');
                nextButton.classList.add('hidden');

            };
            if(tabPic.length > 1)
            {
                previousButton.classList.remove('hidden');
                nextButton.classList.remove('hidden');
    
            };
        };

        function saveToJson(){
            localStorage.setItem('picList', JSON.stringify(tabPic));
            localStorage.setItem('pseudoList', JSON.stringify(tabPseudo));
        };
//////////////////////////////////////////////////////////////////////////////

        function saveUrlToJson(){
            for (let i of textUrlAvatar){
                if(!tabNamePic.includes(i.innerText)){               
                    tabNamePic.push(i.innerText);
                    tabUrlPic.push(i.value); 
                };
            };
            localStorage.setItem('picUrlList', JSON.stringify(tabUrlPic));
            localStorage.setItem('picNameList', JSON.stringify(tabNamePic));
        };
///////////////////////////////////////////////////////////////////////////////
  
    </script>