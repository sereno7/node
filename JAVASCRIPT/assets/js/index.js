// declarer une variable 
let maVariable = 42;

// var maVariable : 43; A eviter;

const MA_CONSTANTE = 44;

// ------------------------------

//type number
let monNombre = 45.56;

//chaine de caractère
let maChaine = 'ma chaine'; `ma Chaine`;"ma Chaine";
// `` utiliser pour concatener avec le $ comme en c# 

// un tableau, pas de type préçis (dynamique comme une liste)
let tableau = [55, 'string', []]

// dictionnaire, pas de type préçis
let dico={
    maCle : 'maValeur',
    maCle2 : 45,
}


// type fonction (proche du delegate)
let fonction = function() {};
let fonction2 = () => {};


// Pas de return specifié dans la fonction 
function  maFonction(a) {

}
// pas necessaire d'utiliser les parametres dans l'appel de la fonction 
maFonction()

//type boolean
let monBooleen = true;

// Afficher une sortie dans la console du browser
console.log(45);

// afficher un tableau dans la console 
console.table(tableau);
console.log(tableau);

// log+to the console raccourcit 

let age = prompt("age?");

age = parseInt(age);

console.log(age+5);


//if identique au c# 

for(let i=0;i<5 i++){

};

while(true){

}

//longueur d'un tableau
tableau.length

// supprimer le dernier element d'un tableau
Array.pop();

// supprimer x element a partir de l'index y
tableau.splice(x,y);

// rechercher l'index de l'élement 42 puis supprimer l'élement à l'index trouvé 
let index = tableau.indexOf(42)
tableau.splice(index,1)



