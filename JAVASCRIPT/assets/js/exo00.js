let nom = document.getElementById('nom');
let prenom = document.getElementById('prenom');
let birthdate = document.getElementById('birthdate');
let btnAdd = document.getElementById('add');

let userTable = document.getElementById('usersList');

// ------------------------------------------
let supprimer = event => {
    let object = event.target;
    let tr = object.parentNode;
    userTable.removeChild(tr);
}
// ------------------------------------------

btnAdd.addEventListener('click',(event) =>{
    
    let TR = document.createElement("tr");

    let TD1  = document.createElement("td");
    TR.appendChild(TD1);

    let TD2  = document.createElement("td");
    TR.appendChild(TD2);

    let TD3  = document.createElement("td");
    TR.appendChild(TD3);
    
    let btn = document.createElement('button');
    btn.innerText="-"
    TR.appendChild(btn);
    btn.addEventListener('click', supprimer);

    userTable.appendChild(TR);
// ------------------------------------------

    let itemNom = document.getElementById('nom');
    TD1.innerText = itemNom.value;

    let itemPrenom = document.getElementById('prenom');
    TD2.innerText = itemPrenom.value;

    let itemBirthdate = document.getElementById('birthdate');
    TD3.innerText = itemBirthdate.value;

    userTable.appendChild(TR);

    prenom.value = null;
    birthdate.value = null;
    nom.value = null;

})
