@Component({
  ...,
  providers: [ DatePipe ]     // Add DatePipe from @angular/common
})
export class SampleComponent  {

   constructor(...,
               private datePipe: DatePipe) {}

   submit() {
     ...
     const body = this.form.value;
     body.dob   = this.datePipe.transform(body.dob, 'yyyy-MM-dd');  // You can provide any date format on the 2nd parameter

     this.ss.register(body).subscribe(res => {...});