import { Film } from "../models/film";
import { Resolve, ActivatedRoute } from "@angular/router";
import { FilmService } from "../services/film.service";
import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
})

export class FilmResolver implements Resolve<Film>{

    constructor(private filmService: FilmService) { }
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
        : Film | import("rxjs").Observable<Film> | Promise<Film> {

        return this.filmService.getDetails(route.params.id);
    }
}


//EXEMPLE ROUTING
////////////////////////////////////////////////////////////
{
    path: 'editSkill/:id',
        component: EditSkillComponent,
            resolve: { resolveSkill: SkillResolver }
},



//EXEMPLE edit-skill-components.ts
////////////////////////////////////////////////////////////
ngOnInit(): void {
    let data = this.route.snapshot.data.resolveSkill;

    //this.model = data;
    this.fg = new FormGroup({
        'Id': new FormControl(data.id)
        ,
        'Name': new FormControl(data.Name, Validators.compose([
            Validators.required,
            Validators.maxLength(50)
        ])),
    });
