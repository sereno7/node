﻿using Forum.ASP.Models;
using Forum.DAL.DI;
using Forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Hosting;

namespace Forum.ASP.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork uow = new UnitOfWork();

        public ActionResult Index()
        {
            RSACryptoServiceProvider serv = new RSACryptoServiceProvider();
            if (System.IO.File.Exists(HostingEnvironment.MapPath("/Keys/keys.xml")))
            {
                string keys = System.IO.File.ReadAllText(HostingEnvironment.MapPath("/Keys/keys.xml"));
                serv.FromXmlString(keys);
            }
            else
            {
                string keys = serv.ToXmlString(true);
                var fs = System.IO.File.Create(HostingEnvironment.MapPath("/Keys/keys.xml"));
                fs.Close();
                System.IO.File.WriteAllText(HostingEnvironment.MapPath("/Keys/keys.xml"), keys);
            }
            IEnumerable<Subject> subjects 
                = uow.GetSubjectRepo().Get();
            IEnumerable<SubjectModel> model
                = subjects.Select(
                s => new SubjectModel
                {
                    Id = s.Id,
                    Title=Encoding.UTF8.GetString(serv.Decrypt(s.Title, false))
                    //Title = s.Title
                });
            return View(model);
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(SubjectModel model)
        {
            if(ModelState.IsValid)
            {
                RSACryptoServiceProvider serv = new RSACryptoServiceProvider();
                if (System.IO.File.Exists(HostingEnvironment.MapPath("/Keys/keys.xml")))
                {
                    string keys = System.IO.File.ReadAllText(HostingEnvironment.MapPath("/Keys/keys.xml"));
                    serv.FromXmlString(keys);
                }
                else
                {
                    string keys = serv.ToXmlString(true);
                    var fs = System.IO.File.Create(HostingEnvironment.MapPath("/Keys/keys.xml"));
                    fs.Close();
                    System.IO.File.WriteAllText(HostingEnvironment.MapPath("/Keys/keys.xml"), keys);
                }
                uow.GetSubjectRepo().Insert(new Subject
                {
                    Id = model.Id,
                    Title = serv.Encrypt(
                        Encoding.UTF8.GetBytes(model.Title), false
                    )
                });
                uow.Save();
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            uow.Dispose();
            base.Dispose(disposing);
        }
    }
}