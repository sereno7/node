﻿using Forum.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.DI
{
    public class UnitOfWork : IDisposable
    {
        private SqlConnection _connection;
        private SqlTransaction _transaction;

        public UnitOfWork()
        {
            string connectionString
                = ConfigurationManager.ConnectionStrings["default"]
                .ConnectionString;
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction 
                = _connection.BeginTransaction();
        }

        public void Dispose()
        {
            //_transaction.Rollback();
            //_transaction.Dispose();
            _connection.Dispose();
        }

        public ResponseRepository GetResponseRepo()
        {
            return new ResponseRepository(_connection);
        }

        public SubjectRepository GetSubjectRepo()
        {
            return new SubjectRepository(_connection, _transaction);
        }

        public bool Save()
        {
            try
            {
                _transaction.Commit();
                return true;
            }
            catch(Exception)
            {
                _transaction.Rollback();
                return false;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
            }
           
        }
    }
}
