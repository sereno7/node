﻿using Forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Repositories
{
    public class SubjectRepository
    {
        private readonly SqlConnection _conn;
        private readonly SqlTransaction _trans;
        public SubjectRepository(
            SqlConnection conn, SqlTransaction trans)
        {
            _conn = conn;
            _trans = trans;
        }

        public IEnumerable<Subject> Get()
        {
            SqlCommand cmd = _conn.CreateCommand();
            cmd.Transaction = _trans;
            cmd.CommandText = "SELECT * FROM [Subject]";
            SqlDataReader r = cmd.ExecuteReader();
            while(r.Read())
            {
                yield return Mappers.Mappers.ToSubject(r);
            }
        }

        public Subject GetById(int id)
        {
            SqlCommand cmd = _conn.CreateCommand();
            cmd.Transaction = _trans;
            cmd.CommandText = "SELECT * FROM [Subject] WHERE Id = @id";
            cmd.Parameters.AddWithValue("id", id);
            SqlDataReader r = cmd.ExecuteReader();
            Subject result = null;
            if (r.Read())
            {
                 result = Mappers.Mappers.ToSubject(r);
            }
            return result;
        }

        public int Insert(Subject s)
        {
            SqlCommand cmd = _conn.CreateCommand();
            cmd.Transaction = _trans;
            cmd.CommandText = "INSERT INTO [Subject] " +
                "OUTPUT INSERTED.Id " +
                "VALUES (@title)";
            cmd.Parameters.AddWithValue("title", s.Title);
            int id = (int)cmd.ExecuteScalar();
            return id;
        }
    }
}
