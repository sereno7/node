﻿CREATE PROCEDURE [dbo].[SP_Check_Password_User]
	@login VARCHAR(63), 
	@password VARCHAR(32),
	@email VARCHAR(254)
AS
	DECLARE @pwd VARBINARY(32)
	DECLARE	@hash VARBINARY(32)
	SELECT @pwd=[Password] From [User] WHERE [Login] = @login OR [Email] = @email;
	SET @hash=dbo.SF_HashPassword(@password);
	IF(@pwd = @hash) BEGIN SELECT 1 END ELSE BEGIN SELECT 0 END;
	
--     SELECT [Id] FROM Users where [Password] = dbo.SF_HashPassword(@password) AND ([Login] = @login OR [Email] = @email)



