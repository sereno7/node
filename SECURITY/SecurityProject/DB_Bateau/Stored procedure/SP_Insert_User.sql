﻿CREATE PROCEDURE [dbo].[SP_Insert_User]
	@login VARCHAR(63),
	@password VARCHAR(32),
	@email VARCHAR(254)
AS
	INSERT INTO [User]([Login],[Password],[Email])
	--renvoyer l'ID depuis la table inserted
	OUTPUT inserted.Id
	VALUES (@login,dbo.SF_HashPassword(@password),@email);
