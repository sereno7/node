﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conso_Bateau
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ConString = @"Data Source=5246\SQLSERVER;Initial Catalog=DB_Bateau;Persist Security Info=True;User ID=sa;Password=Test1234=";
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    //command.CommandText = "EXEC SP_INSERT_User @login = @identifiant, @password = @pwd, @email = @mail";
                    command.CommandText = "SP_Insert_User";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter Plogin = new SqlParameter();
                    Plogin.Value = "tètè";
                    Plogin.ParameterName = "login";
                    SqlParameter Ppwd = new SqlParameter("password", "Test1234=");
                    command.Parameters.Add(Plogin);
                    command.Parameters.Add(Ppwd);
                    command.Parameters.AddWithValue("email", "tété@ou.titi");

                    connection.Open();
                    Console.WriteLine((int)command.ExecuteScalar());
                }
            }
            Console.ReadLine();
        }
    }
}
