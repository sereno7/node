﻿using forum.DAL.Entities;
using forum.DAL.Repositories;
using SecurityForumEXO.DI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SecurityForumEXO.Models
{
    public class SubjectModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public IEnumerable<ReplyModel> listReply 
        { 
            get
            {
                ReplyRepository repo = ServicesLocator.GetService<ReplyRepository>();
                IEnumerable<Reply> listReply = repo.Get().Where((x) => x.idSubject == id);
                return listReply.Select(x => new ReplyModel
                {
                    //id = x.id,
                    reply = x.reply,
                    postedDate = x.postedDate.Value,
                    author = x.author,
                    idSubject = x.idSubject
                });
            }
        }
    }
}