﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityForumEXO.Models
{
    public class ReplyModel
    {
        //public int id { get; set; }
        public string reply { get; set; }
        public DateTime postedDate { get; set; }
        public string author { get; set; }
        public int idSubject { get; set; }
    }
}