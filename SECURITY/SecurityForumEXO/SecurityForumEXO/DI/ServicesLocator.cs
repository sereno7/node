﻿using forum.DAL.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace SecurityForumEXO.DI
{
    public class ServicesLocator
    {
        private static ServiceCollection Services = new ServiceCollection();
        private static ServiceProvider Provider;
        static ServicesLocator()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider = ConfigurationManager.ConnectionStrings["default"].ProviderName;
            Services.AddTransient(
                (x) => new SubjectRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new ReplyRepository(connectionstring, provider)
            );
            Provider = Services.BuildServiceProvider();
        }
        public static T GetService<T>()
        {
            return Provider.GetService<T>();
        }
    }
}