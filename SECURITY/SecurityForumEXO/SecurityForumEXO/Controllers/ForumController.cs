﻿using forum.DAL.Entities;
using forum.DAL.Repositories;
using SecurityForumEXO.DI;
using SecurityForumEXO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecurityForumEXO.Controllers
{
    public class ForumController : Controller
    {
        // GET: Forum
        public ActionResult Index()
        {
            SubjectRepository repo = ServicesLocator.GetService<SubjectRepository>();
            IEnumerable<Subject> listSubject = repo.Get();
            IEnumerable<SubjectModel> model = listSubject.Select(x => new SubjectModel
            {
                id = (int)x.id,
                name = x.name as string,
            });
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.idPage = id;
            SubjectRepository repo = ServicesLocator.GetService<SubjectRepository>();
            Subject Subject = repo.GetById(id);
            SubjectModel Model = new SubjectModel
            {
                id = (int)Subject.id,
                name = Subject.name as string,
            };
            return View(Model);
        }

        [HttpPost]
        public ActionResult Edit(ReplyModel model)
        {
            if (ModelState.IsValid)
            {
                ReplyRepository repo = ServicesLocator.GetService<ReplyRepository>();
                Reply s = new Reply
                {
                    //id = model.id,
                    reply = model.reply,
                    author = model.author,
                    idSubject = model.idSubject,
                    postedDate = DateTime.Now
                };               
                repo.Insert(s);
                return RedirectToAction("Index", "Forum");
            }          
            return View(model);
        }
    }
}