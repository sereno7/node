﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace forum.DAL.Entities
{
    public class Reply
    {
        //public int id { get; set; }
        public string reply { get; set; }
        public DateTime? postedDate { get; set; }
        public string author { get; set; }

        public int idSubject { get; set; }
    }
}
