﻿using forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace forum.DAL.Repositories
{
    public class SubjectRepository : BaseRepository<Subject>
    {
        public SubjectRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
