﻿using API.Models;
using API.Utils.Security.RSA;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private KeyGenerator _keyGenerator;
        private Decrypting _decrypting = new Decrypting();

        private User _userDB = new User();
        private User _fakeUser = new User();

        public UserController(KeyGenerator keyGenerator)
        {
            _keyGenerator = keyGenerator;

        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("Login")]
        public IActionResult Login(User user)
        {
            if(user.login == _userDB.login)
            {
                string PrivateKey = _keyGenerator.PrivateKey;
                user.password = _decrypting.Decrypt(user.password, PrivateKey);

                if (user.password == _userDB.password)
                {
                   //fake recup bsd + verif 
                    //OK !
                    return Ok(_userDB.name);
                }
            }

            return NoContent();
        }


        [HttpGet("GetPublicKey")]
        public IActionResult GetPublicKey()
        {
            _keyGenerator.GenerateKeys(RSAKeySize.Key2048);
            string publicKey = _keyGenerator.PublicKey;

            if (publicKey != null)
                return Ok(publicKey);
            else
                return NotFound();
        }
    }
}
