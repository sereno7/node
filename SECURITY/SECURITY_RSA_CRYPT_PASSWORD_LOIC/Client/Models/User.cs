﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Models
{
    public class User
    {
        [Required(ErrorMessage = "Login obligatoire")]
        [DisplayName("Login")]
        [DataType(DataType.Text)]
        public string login { get; set; }

        [Required(ErrorMessage = "Password obligatoire")]
        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        public string name { get; set; }
    }
}
