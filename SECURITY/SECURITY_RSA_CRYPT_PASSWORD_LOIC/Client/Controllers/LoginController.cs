﻿using Client.Models;
using Client.Utils;
using Client.Utils.Security.RSA;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private CryptingRSA _cryptingRSA;
        private ControllerAPI _controllerAPI;

        public LoginController(CryptingRSA cryptingRSA, ControllerAPI controllerAPI)
        {
            _cryptingRSA = cryptingRSA;
            _controllerAPI = controllerAPI;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new User());
        }

        [HttpPost]
        public IActionResult Index([FromForm] User user)
        {
            if(ModelState.IsValid)
            {
                string publicKey = _controllerAPI.GetPublicKey();
                user.password = _cryptingRSA.Encrypt(user.password, publicKey);

                user.name = _controllerAPI.Login<User>(user);

            }

            return View(user) ;
        }

    }
}
