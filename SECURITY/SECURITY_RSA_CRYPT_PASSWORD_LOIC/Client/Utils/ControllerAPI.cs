﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace Client.Utils
{
    public class ControllerAPI
    {
        protected HttpClient _client;
        private Uri _baseUrl = new Uri("https://localhost:5001/api/");


        public ControllerAPI()
        {
            HttpClientHandler handler = new HttpClientHandler() { SslProtocols = SslProtocols.Tls12 };
            //la configuration de l'api étant sur le console et non en applicatif, le protocole ssl doit être en SSL12 (ssl 1.2)
            handler.ServerCertificateCustomValidationCallback = (request, cert, chain, errors) => { return true; };
            _client = new HttpClient(handler);

            _client.BaseAddress = _baseUrl;
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        public string Login<TEntity>(TEntity model)
        {
            string json = JsonConvert.SerializeObject(model);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            using (HttpResponseMessage response = _client.PostAsync("User/Login", content).Result)
            {
                response.EnsureSuccessStatusCode();
                string Json = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<string>(Json);
            }
        }


        public string GetPublicKey()
        {
            using (HttpResponseMessage response = _client.GetAsync("User/GetPublicKey").Result)
            {
                try
                {
                    response.EnsureSuccessStatusCode();
                    string Json = response.Content.ReadAsStringAsync().Result;

                    return JsonConvert.DeserializeObject<string>(Json);
                }
                catch (Exception e)
                {
                    return "Une erreur est survenue lors de l'appel de L'API";
                }

            }
        }
    }
    
}
