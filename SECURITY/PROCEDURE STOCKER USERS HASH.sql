CREATE TABLE [User](
	Id INT PRIMARY KEY IDENTITY,
	[Name] VARCHAR(50) NOT NULL,
	[Email] VARCHAR(255) NOT NULL,
	[Password] VARBINARY(MAX) NOT NULL,
	[Role] VARCHAR(50) NOT NULL,
);

GO
CREATE PROCEDURE SP_Register
	@Name VARCHAR(50),
	@Email VARCHAR(255),
	@PlainPassword VARCHAR(255),
	@Role VARCHAR(50)
AS BEGIN
	INSERT INTO [User]([Name], Email, [Password], [Role])
	VALUES (@Name, @Email, HASHBYTES('SHA2_512', @PlainPassword), @Role)
END

GO
CREATE PROCEDURE SP_Login
	@Login VARCHAR(255),
	@PlainPassword VARCHAR(255)
AS
BEGIN
	SELECT * FROM [User]
	WHERE Email = @Email AND [Password] = HASHBYTES('SHA2_512', @PlainPassword)
END

