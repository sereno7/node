﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http.Dependencies;
using ToolBox.JWT;

namespace Token.DI
{
    public class ServiceLocator : IDependencyResolver
    {
        private readonly ServiceCollection _services;
        private readonly ServiceProvider _provider;

        public ServiceLocator()
        {
            _services = new ServiceCollection();
            //Ici vous pouvez enregistrer tout vos services
            _services.AddScoped<TokenService>();
            AddControllers(_services);

            //build du serviceProvider
            _provider = _services.BuildServiceProvider();
        }

        private void AddControllers(ServiceCollection services)
        {
            IEnumerable<Type> types = Assembly.GetExecutingAssembly()
                .GetTypes().Where(t => t.Name.EndsWith("Controller"));
            foreach (var type in types)
            {
                services.AddScoped(type);
            }
        }

        public IDependencyScope BeginScope()
        {
            return new ServiceLocator();
        }

        public void Dispose()
        {
            (_provider as IDisposable)?.Dispose();
        }

        public object GetService(Type serviceType)
        {
            return _provider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _provider.GetServices(serviceType);
        }
    }
}