﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using ToolBox.JWT;

namespace Token.Filters
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues(
                "Authorization", out IEnumerable<string> headers
                );
            if (headers == null) throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            string token = headers.FirstOrDefault(x => x.StartsWith("Bearer "))
                ?.Replace("Bearer ", "");
            if (token == null)
                throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            TokenService s = GlobalConfiguration.Configuration.DependencyResolver
                .GetService(typeof(TokenService)) as TokenService;
            ClaimsPrincipal p = s.Decode(token);
            if (p == null)
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            if(p.Claims.FirstOrDefault(c => c.Type == "RoleName")?.Value != "ADMIN")
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            //if (!p.IsInRole("ADMIN"))
            //    throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }
    }
}