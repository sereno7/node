﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CONSOLE_DemoBindingPerso
{
    class Program
    {
        static void Main(string[] args)
        {
            Fenetre f = new Fenetre();
            f.DataContext = new DataContext() { Message = "Hello World !!" };
            f.Show();
            Console.ReadKey();
        }
    }
}
