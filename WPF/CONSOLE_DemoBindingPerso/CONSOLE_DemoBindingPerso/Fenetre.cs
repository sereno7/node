﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CONSOLE_DemoBindingPerso
{
    class Fenetre
    {
        public DataContext DataContext { get; set; }

        public void Show()
        {
            Console.SetCursorPosition(5, 5);
            Console.WriteLine(DataContext.Message);

        }
    }
}
