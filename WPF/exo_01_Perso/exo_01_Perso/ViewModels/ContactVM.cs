﻿using exo_01_Perso.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;

namespace exo_01_Perso.ViewModels
{
    class ContactVM : BindableBase
    {
        private string _nom;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; RaisePropertyChanged(); }
        }

        private string _telephone;
        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value; RaisePropertyChanged(); }
        }

        private ObservableCollection<Contact> _liste;

        public ObservableCollection<Contact> Liste
        {
            get { return _liste; }
            set { _liste = value; RaisePropertyChanged();}
        }



        public RelayCommand AddCmd { get; set; }
        public RelayCommand<Contact> DelCmd { get; set; }

        public ContactVM()
        {
            Liste = new ObservableCollection<Contact>();
            AddCmd = new RelayCommand(Add);
            DelCmd = new RelayCommand<Contact>(Del);
        }

        public void Add()
        {
            Liste.Add(new Contact
            {
                Nom = this.Nom,
                Telephone = this.Telephone,
            });
            this.Nom = null;
            this.Telephone = null;
        }
        private void Del(Contact c)
        {
            Liste.Remove(c);
        }


    }
}
