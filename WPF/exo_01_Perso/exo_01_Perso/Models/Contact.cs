﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo_01_Perso.Models
{
    class Contact
    {
        public string Nom { get; set; }
        public string Telephone { get; set; }
    }
}
