﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace TB_WPF_DEMO3_PERSO
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int size;
        private string message;
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                // Quand la propriété est modifiée, on remet a jour la vue
                message = value;
                RaisePropertyChanged();
            }
        }
        public int Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
                RaisePropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<User> Collection { get; set; }

        public MainWindow()
        {
            Collection = new ObservableCollection<User>
            {
                new User {Name ="Mike", Age =38, BirthDate = new DateTime(1982,3,17), IsYellow = false},
                new User {Name ="Khun", Age =38, BirthDate = new DateTime(1982,5,6), IsYellow = true},
                new User {Name ="Piv", Age =39, BirthDate = new DateTime(1981,4,30), IsYellow = true},
            };
            Message = "Hello World";
            Size = 40;
            DataContext = this;
            InitializeComponent();
            //Button btn = new Button();
            //btn.Content = "Click Me !!!";
            //Content = btn;
            //btn.Click += (sender, e) =>
            //{
            //    Button b = (Button)sender;
            //};
        }

        

        private void OnClick(object sender, RoutedEventArgs e)
        {
            Message = "Le message a changé";
            //Ouvrir une nouvelle fenetre
            //Page2 page = new Page2();
            //page.Show();
            //this.Close();

            //Boite de dialogue classique
            //MessageBoxResult result = 
            //    MessageBox.Show("Hello World!", "titre de messageBox", MessageBoxButton.YesNoCancel);
            //if(result == MessageBoxResult.Yes)
            //{
            //    //DO
            //}
            //else if(result == MessageBoxResult.No)
            //{
            //    //DO
            //}
            //else
            //{
            //    //DO
            //}

            //Ouverture fichier
            //OpenFileDialog dlg = new OpenFileDialog();
            //dlg.ShowDialog();

            //sauvegarder un fichier en local
            //SaveFileDialog slg = new SaveFileDialog();
            //slg.ShowDialog();

            // Boite dialogue d'impression
            //PrintDialog plg = new PrintDialog();
            //plg.ShowDialog();
        }
        //methode pour mettre a jour la vue apres modification d'une variable
        //CallerMemberName sert a utiliser la nom de la variable en question comme parametre
        protected void RaisePropertyChanged([CallerMemberName]string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        private void Click(object sender, RoutedEventArgs e)
        {
            Size++;
        }

        private void ClicMinus(object sender, RoutedEventArgs e)
        {
            Size--;
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            Collection.Add(new User
            {
                Name = "Steve",
                IsYellow = false,
                Age = 32,
                BirthDate = new DateTime(1988,5,5)
            });
        }
    }
}
