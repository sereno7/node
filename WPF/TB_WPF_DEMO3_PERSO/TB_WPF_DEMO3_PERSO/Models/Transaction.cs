﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TB_WPF_DEMO3_PERSO.Models
{
    public class Transaction
    {
        public double Montant { get; set; }
        public string Objet { get; set; }
        public bool IsDepense { get; set; }
    }
}
