﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TB_WPF_DEMO3_PERSO.Models;
using ToolBox.MVVM.BaseClasses;

namespace TB_WPF_DEMO3_PERSO.ViewModels
{
    public class BudgetVM : BindableBase
    {
        private double _montant;

        public double Montant
        {
            get { return _montant; }
            set { _montant = value; RaisePropertyChanged(); }
        }

        public string Objet { get; set; }
        public bool IsDepense { get; set; }

        private double _total;

        public double Total
        {
            get { return _total; }
            set
            {
                _total = value;
                RaisePropertyChanged();
            }
        }
        public ObservableCollection<Transaction> Items { get; set; }
        public RelayCommand AddCmd { get; set; }

        public BudgetVM()
        {
            Items = new ObservableCollection<Transaction>();
            AddCmd = new RelayCommand(Add);
        }

        public void Add()
        {
            Items.Add(new Transaction
            {
                Montant = this.Montant,
                Objet = this.Objet,
                IsDepense = this.IsDepense
            });

            //SI ce n'est pas une dépense -> ++ sinon --
            Total += (!this.IsDepense) ? this.Montant : -1 * this.Montant;

            this.Montant = 0;
            this.Objet = null;
            this.IsDepense = false;
            RaisePropertyChanged("Objet");
            RaisePropertyChanged("IsDepense");
        }
    }
}
