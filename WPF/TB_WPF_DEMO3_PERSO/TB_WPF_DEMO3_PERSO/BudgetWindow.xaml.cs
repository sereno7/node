﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TB_WPF_DEMO3_PERSO.Models;
using TB_WPF_DEMO3_PERSO.ViewModels;

namespace TB_WPF_DEMO3_PERSO
{
    /// <summary>
    /// Logique d'interaction pour BudgetWindow.xaml
    /// </summary>
    public partial class BudgetWindow : Window
    {
        

        public BudgetWindow()
        {
            DataContext = new BudgetVM();
            InitializeComponent();
        }
    }
}
