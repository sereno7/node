﻿using exo_chat_perso.Models;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;

namespace exo_chat_perso.ViewModels
{
    class MainViewModel : BindableBase
    {
		private string _content;

		public string Content
		{
			get { return _content; }
			set { _content = value; RaisePropertyChanged();}
		}

		private ObservableCollection<Message> _Messages;

		public ObservableCollection<Message> Messages
		{
			get { return _Messages; }
			set { _Messages = value; }
		}

		public RelayCommand SendCmd
		{
			get
			{
				return new RelayCommand(() =>
				{
					//Connecter au hub
					//Envoyer le message
					if (token == null) return;
					_hub.Invoke("SendToAll", new Message
					{
						Author = "Moi",
						Content = Content,
						Date = DateTime.Now,
						Token = token
					});
				});
			}
		}
		private HubConnection _conn;
		private IHubProxy _hub;

		private string token;

		public MainViewModel()
		{
			Messages = new ObservableCollection<Message>();
			_conn = new HubConnection("http://khun.somee.com/signalr");
			_hub = _conn.CreateHubProxy("MessageHub");
			_conn.Start().Wait();
			_hub.On<Message>("NEwMessage", (m) => {
				//ajouter le message dans la collection
				//appeler le dispatcher pour retourner dans le thread principal
				
				App.Current.Dispatcher.Invoke(() =>
				{
					Messages.Add(m);
				});
			});

			_hub.On<string>("NewToken", t => {
				App.Current.Dispatcher.Invoke(() =>
				{
					token = t;
				});
			});

			_hub.Invoke("SignIn",
				new Login { Username = "Sereno", Password = "ciney" }
				);
		}
	}
}
