﻿using Newtonsoft.Json;
using Pokedex_Perso.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex_Perso.Services
{
    class pokemonService
    {
        public IndexPokemon GetIndex(string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                //recupere une reponse 
                HttpResponseMessage response =  client.GetAsync(uri).Result; //.result nous fait passer en synchrone
                
                if (response.IsSuccessStatusCode) //Si la reponse est valide
                {
                    //Recupere le contenu de la reponse
                    HttpContent Content = response.Content;
                    string json = Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IndexPokemon>(json);
                }
                return null;
            };
            
        }

        public DetailsPokemon GetDetail(string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                //recupere une reponse 
                HttpResponseMessage response = client.GetAsync(uri).Result; //.result nous fait passer en synchrone

                if (response.IsSuccessStatusCode) //Si la reponse est valide
                {
                    //Recupere le contenu de la reponse
                    HttpContent Content = response.Content;
                    string json = Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<DetailsPokemon>(json);
                }
                return null;
            };

        }
    }
}
