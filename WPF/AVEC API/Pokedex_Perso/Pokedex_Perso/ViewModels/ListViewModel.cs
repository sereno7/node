﻿using Pokedex_Perso.Models;
using Pokedex_Perso.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;
using ToolBox.MVVM.Mediator;

namespace Pokedex_Perso.ViewModels
{
    class ListViewModel : BindableBase
    {
		private ObservableCollection<SimplePokemon> _pokemons;

		public ObservableCollection<SimplePokemon> Pokemons
		{
			get { return _pokemons; }
			set { _pokemons = value; RaisePropertyChanged(); }
		}

		public string Prev { get; set; }
		public string Next { get; set; }
		public RelayCommand<string> DetailsCommand { get; set; }
		public RelayCommand PrevCommand { get; set; }
		public RelayCommand NextCommand { get; set; }

		private int Count;



		public ListViewModel()
		{
			pokemonService pService = new pokemonService();
			IndexPokemon index = pService.GetIndex("https://pokeapi.co/api/v2/pokemon/");
			Pokemons = new ObservableCollection<SimplePokemon>(index.results);
			Count = index.Count;
			Prev = index.Previous;
			Next = index.Next;
			DetailsCommand = new RelayCommand<string>(Details);
			PrevCommand = new RelayCommand(GoPrev);
			NextCommand = new RelayCommand(GoNext);
		}

		public void Details(string url)
		{
			Messenger<string>.Instance.Publish("RefreshPokemon", url);
		}

		public int Offset { get; set; } = 0;
		public void GoPrev() {
			if(Offset -20 >= 0)
			{
				Offset -= 20;
				pokemonService pS = new pokemonService();
				IndexPokemon index = pS.GetIndex("https://pokeapi.co/api/v2/pokemon?limit=20&offset=" + Offset);
				//IndexPokemon index = pS.GetIndex(Next);
				Pokemons = new ObservableCollection<SimplePokemon>(index.results);
			}
		}
		public void GoNext()
		{
			if (Offset + 20 < Count)
			{
				Offset += 20;
				pokemonService pS = new pokemonService();
				IndexPokemon index = pS.GetIndex("https://pokeapi.co/api/v2/pokemon?limit=20&offset=" + Offset);
				Pokemons = new ObservableCollection<SimplePokemon>(index.results); 
			}
		}
	}
}
