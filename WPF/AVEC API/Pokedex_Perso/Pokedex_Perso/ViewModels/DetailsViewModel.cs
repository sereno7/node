﻿using Pokedex_Perso.Models;
using Pokedex_Perso.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;
using ToolBox.MVVM.Mediator;

namespace Pokedex_Perso.ViewModels
{
    class DetailsViewModel : BindableBase
    {
        private DetailsPokemon _detailsPokemon;

        public DetailsPokemon DetailsPokemon
        {
            get { return _detailsPokemon; }
            set { _detailsPokemon = value;RaisePropertyChanged(); }
        }
        public DetailsViewModel()
        {
            Messenger<string>.Instance.Subscribe("RefreshPokemon",(url)=> {
                pokemonService pService = new pokemonService();
                DetailsPokemon details = pService.GetDetail(url);
                DetailsPokemon = details;
            });
        }

    }
}
