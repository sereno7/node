﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex_Perso.Models
{
    class SimplePokemon
    {
        public string Name { get; set; }
        public string Url { get; set; }

    }
}
