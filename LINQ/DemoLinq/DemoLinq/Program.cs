﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLinq
{
    class Person
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public void Parler() { Console.WriteLine("Salut"); }
    }

    static class PersonExtension
    {
        public static void Sexcuser(this Person p) { Console.WriteLine("Pardon"); }
    }

    static class ListExtension
    {
        public static List<int> FilterPair(this List<int> l)
        {
            List<int> result = new List<int>();
            foreach (int item in l)
            {
                if (item % 2 == 0)
                    result.Add(item);
            }
            return result;
        }
    }

    //static class ListExtension2
    //{
    //    public static IE<int> FilterPair(this List<int> l)
    //    {
    //        List<int> result = new List<int>();
    //        foreach (int item in l)
    //        {
    //            if (item % 2 == 0)
    //                result.Add(item);
    //        }
    //        return result;
    //    }
    //}



    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<int> List = new List<int> { 1, 2, 3 };
            //List.Where
            Person p = new Person();
            p.Parler();
            p.Sexcuser();
            Console.ReadKey();
        }
    }
}
