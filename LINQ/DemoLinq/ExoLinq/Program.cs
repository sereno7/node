﻿using LINQDataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            DataContext dc = new DataContext();


            //var l = dc.Students.Where(x => x.Year_Result > 18);
            //foreach (Student s in l)
            //{
            //    Console.WriteLine(s.First_Name);
            //}

            //EXO2.1
            //var ex = dc.Students
            //    .Select(x => new {
            //        Nom = x.Last_Name,
            //        DateNaiss = x.BirthDate,
            //        Resultat = x.Year_Result,
            //        Login = x.Login
            //    });
            //foreach (var s in ex)
            //{
            //    Console.WriteLine($"{s.Nom} - {s.Login} - {s.DateNaiss} - {s.Resultat}");
            //}

            //EXO2.2
            //var ex = dc.Students
            //    .Select(x => new
            //    {
            //        Nom = x.Last_Name,
            //        Prenom = x.First_Name,
            //        Id = x.Student_ID,
            //        DateNaiss = x.BirthDate
            //    });
            //foreach (var s in ex)
            //{
            //    Console.WriteLine($"{s.Nom} {s.Prenom} - {s.Id} - {(s.DateNaiss).ToShortDateString()}");
            //}

            //EXO2.3
            //var ex = dc.Students
            //    .Select(x => new
            //    {
            //        Nom = x.Last_Name,
            //        Prenom = x.First_Name,
            //        x.Student_ID,
            //        DateNaiss = x.BirthDate,
            //        Login = x.Login,
            //        x.Section_ID,
            //        x.Year_Result,
            //        x.Course_ID
            //    });
            //foreach (var s in ex)
            //{
            //    Console.WriteLine($"{s.Nom} | {s.Prenom} | {s.Student_ID} | {(s.DateNaiss).ToShortDateString()}| {s.Login} | {s.Section_ID} | {s.Year_Result} | {s.Course_ID}");
            //};

            //EXO3.1

            //var ex = dc.Students
            //    .Where(x => x.BirthDate.Year < 1955)
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.Year_Result,
            //        Status = (x.Year_Result > 12) ? "ok" : "ko"
            //    });

            //foreach (var s in ex)
            //{
            //    Console.WriteLine($"{s.Last_Name} - {s.Year_Result} - {s.Status}");
            //}

            //EXO3.2

            //var ex = dc.Students
            //    .Where(x => x.BirthDate.Year > 1954 && x.BirthDate.Year < 1966)
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.Year_Result,
            //        Categorie = (x.Year_Result < 10) ? "Inf" : (x.Year_Result > 10 ? "Sup" : "Neutre")
            //    }
            //    );
            //foreach (var s in ex)
            //{
            //    Console.WriteLine($"{s.Last_Name} - {s.Year_Result} - {s.Categorie}");
            //}

            //EXO3.3
            //var ex = dc.Students
            //    .Where(x => x.Last_Name.EndsWith("r"))
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.Section_ID
            //    });
            //foreach (var item in ex)
            //{
            //    Console.WriteLine($"{item.Last_Name} {item.Section_ID}");
            //}

            //EXO3.4
            //var ex = dc.Students
            //    .Where(x => x.Year_Result <= 3)
            //    .OrderByDescending(x => x.Year_Result)
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.Year_Result
            //    });

            //foreach (var item in ex)
            //{
            //    Console.WriteLine($"{item.Last_Name} {item.Year_Result}");
            //}

            //EXO3.5
            //var ex = dc.Students
            //    .Where(x => x.Section_ID == 1110)
            //    .OrderBy(x => x.Last_Name)
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.First_Name,
            //        x.Year_Result

            //    });

            //foreach(var item in ex)
            //{
            //    Console.WriteLine($"{item.Last_Name} {item.First_Name} - {item.Year_Result}");
            //}

            //EXO3.6
            //var ex = dc.Students
            //    .Where(x => x.Section_ID == 1020 || x.Section_ID == 1010)
            //    .Where(x => x.Year_Result > 18 || x.Year_Result < 12)
            //    .OrderBy(x => x.Section_ID)
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.Section_ID,
            //        x.Year_Result
            //    });

            //foreach (var item in ex)
            //{
            //    Console.WriteLine($"{item.Last_Name} {item.Section_ID} {item.Year_Result}");
            //}

            //EXO3.7
            //var ex = dc.Students
            //    .Where(x => (x.Year_Result * 5 <= 60 && x.Section_ID.ToString().StartsWith("13")))
            //    .OrderByDescending(x => x.Year_Result)             
            //    .Select(x => new
            //    {
            //        x.Last_Name,
            //        x.Section_ID,
            //        result_100 = x.Year_Result * 5
            //    });

            //foreach (var item in ex)
            //{
            //    Console.WriteLine($"{item.Last_Name} {item.Section_ID} {item.result_100}"); 
            //}

            //////////////////////////
            ///JOINTURE EXEMPLE
            //////////////////////////
            ///

            //var ex2 = dc.Students.Join(
            //    dc.Sections,
            //    st => st.Section_ID,
            //    se => se.Section_ID,
            //    (st, se) => new
            //    {
            //        st.Last_Name,
            //        se.Section_Name
            //    }
            //);

            //double ex41 = dc.Students.Average(x => x.Year_Result);
            //Console.WriteLine(ex41);

            //double ex42 = dc.Students.Max(x => x.Year_Result);
            //Console.WriteLine(ex42);


            //double ex43 = dc.Students.Sum(x => x.Year_Result);
            //Console.WriteLine(ex43);

            //double ex44 = dc.Students.Min(x => x.Year_Result);
            //Console.WriteLine(ex44);

            //double ex45 = dc.Students.Count(x => x.Year_Result % 2 != 0);        
            //Console.WriteLine(ex45);


            //var ex51 = dc.Students
            //    .GroupBy(x => x.Section_ID)
            //    .Select(x => new
            //    {
            //        SectionId = x.Key,
            //        Max = x.Max(y => y.Year_Result)
            //    });

            //foreach (var item in ex51)
            //{
            //    Console.WriteLine(item.SectionId + " " + item.Max);
            //}

            //var ex52 = dc.Students
            //    .GroupBy(x => x.Section_ID)
            //    .Where(x => x.Key.ToString().StartsWith("10"))
            //    .Select(x => new
            //    {
            //        SectionId = x.Key,
            //        AVGResult = x.Average(y => y.Year_Result)
            //    });
            //foreach (var item in ex52)
            //{
            //    Console.WriteLine(item.SectionId + " " + item.AVGResult);
            //}

            //var ex53 = dc.Students
            //    .Where(x => x.BirthDate.Year < 1986 && x.BirthDate.Year < 1969)
            //    .GroupBy(x => x.BirthDate.Month)
            //    .Select(x => new
            //    {
            //        AVGResult = x.Average(y => y.Year_Result),
            //        BirthMonth = x.Key

            //    });
            //foreach (var item in ex53)
            //{
            //    Console.WriteLine(item.AVGResult +" "+ item.BirthMonth);
            //}


            //var ex54 = dc.Students
            //    .GroupBy(x => x.Section_ID)
            //    .Where(x => x.Count() > 3)
            //    .Select(x => new
            //    {
            //        Section = x.Key,
            //        AVGResult = x.Average(y => y.Year_Result)
            //    });

            //foreach (var item in ex54)
            //{
            //    Console.WriteLine(item.Section + " " + item.AVGResult);
            //}

            //var ex55 = dc.Professors
            //    .Join(
            //        dc.Courses,
            //        p => p.Professor_ID,
            //        c => c.Professor_ID,
            //        (p, c) => new
            //        {
            //            c.Course_Name,
            //            p.Professor_Name,
            //            ID = p.Section_ID
            //        }
            //    )
            //    .Join(
            //        dc.Sections,
            //        p => p.ID,
            //        s => s.Section_ID,
            //        (p, s) => new
            //        {
            //            p.Course_Name,
            //            p.Professor_Name,
            //            s.Section_Name
            //        })
            //    ;

            //foreach (var item in ex55)
            //{
            //    Console.WriteLine(item.Course_Name + " " + item.Professor_Name + " " + item.Section_Name);
            //}

            //var ex56 = dc.Sections
            //.Join(
            //    dc.Students,
            //    se => se.Delegate_ID,
            //    st => st.Student_ID,
            //    (se, st) => new
            //    {
            //        se.Section_ID,
            //        se.Section_Name,
            //        st.Last_Name
            //    }
            //);


            //foreach (var item in ex56)
            //{
            //    Console.WriteLine(item.Section_ID + " " + item.Section_Name + " " + item.Last_Name);
            //}

            //var ex57 = dc.Sections
            //    .GroupJoin(
            //        dc.Professors,
            //        se => se.Section_ID,
            //        prof => prof.Section_ID,
            //        (se, prof) => new
            //        {
            //            se.Section_ID,
            //            se.Section_Name,
            //            test = prof
            //        });

            //foreach (var item in ex57)
            //{
            //    Console.WriteLine(item.Section_ID +" " + item.Section_Name);
            //    foreach (var p in item.test)
            //    {
            //        Console.WriteLine(p.Professor_Name);                
            //    }
            //}

            //var ex58 = dc.Sections               
            //    .GroupJoin(
            //        dc.Professors,
            //        se => se.Section_ID,
            //        prof => prof.Section_ID,
            //        (se, prof) => new
            //        {
            //            se.Section_ID,
            //            se.Section_Name,
            //            test = prof
            //        })
            //    .Where(x => x.test.Count() > 0);

            //foreach (var item in ex58)
            //{
            //    Console.WriteLine(item.Section_ID + " " + item.Section_Name);
            //    foreach (var p in item.test)
            //    {
            //        Console.WriteLine(p.Professor_Name);
            //    }
            //}

            var Result49Bis = dc.Students
                .Join(dc.Grades, st => true, gr => true, (st, grade) => new { Student = st, Grade = grade })
                                         
                .Where(join => join.Student.Year_Result >= 12 && join.Student.Year_Result >= join.Grade.Lower_Bound && join.Student.Year_Result <= join.Grade.Upper_Bound)
                                         
                .Select(join => new { join.Student.Last_Name, join.Student.Year_Result, Grade = join.Grade.GradeName })
                                         
                .OrderBy(elt => elt.Grade);




            Console.ReadKey();








        }
    }
}
