--Exercice 2.7.1 

SELECT
	last_name,
	first_name,
	section_id
FROM student
WHERE section_id = (
	SELECT section_id
	FROM student
	WHERE last_name LIKE 'Roberts'
	)
	AND NOT last_name LIKE 'Roberts'

---------------------------------------
--Exercice 2.7.2 
SELECT 
	last_name,
	first_name,
	year_result
FROM student
WHERE year_result > 2*(
	SELECT AVG(year_result)
	FROM student
	)

---------------------------------------
--Exercice 2.7.3
SELECT 
	section_id,
	section_name
FROM section
WHERE section_id 
	not in (
	SELECT section_id 
	FROM professor
)

---------------------------------------

-- Exercice 2.7.4 

SELECT
	*
FROM student
WHERE MONTH(birth_date) = (
	SELECT MONTH(professor_hire_date)
	FROM professor
	WHERE professor_name LIKE 'Giot'
)
ORDER BY year_result DESC

--------------------------------------
--Exercice 2.7.5 
SELECT *
FROM student
WHERE year_result BETWEEN (
	SELECT lower_bound
	FROM grade
	WHERE grade LIKE 'TB'
)
AND (
	SELECT upper_bound
	FROM grade
	WHERE grade LIKE 'TB'
)

----------------------------------------

--Exercice 2.7.6

SELECT 
	last_name,
	first_name,
	section_id
FROM student
WHERE section_id = (
	SELECT
		section_id
	FROM section
	WHERE delegate_id = (
		SELECT student_id
		FROM student
		WHERE last_name LIKE 'Marceau'
		)
)
-----------------------------------------
-- SOLUTION MATHIEU

select
    last_name,
    first_name,
    ST.section_id
from student as ST
where section_id = (select ST.section_id from student as ST inner join section as SE on SE.delegate_id = ST.student_id where last_name like 'Marceau')

---------------------------------------

-- Exercice 2.7.7

SELECT 
	section_id,
	section_name
FROM section
WHERE (
	SELECT 
		COUNT(student_id)
	FROM student
) > 4

-----------------------------------

-- SOLUTION MATHIEU

select
    section_id,
    section_name
from section as SE
where (
select count(last_name) 
from student 
where section_id = se.section_id) > 4


-------------------------------------

-- Exercice 2.7.8 

SELECT last_name, first_name, section_id
FROM student s
WHERE year_result = (
	SELECT 
		MAX(year_result)
	FROM student
	WHERE section_id = s.section_id
	GROUP BY section_id
	HAVING AVG(year_result) >= 10
)
---------------------------------------

-- Exercice 2.7.9 

SELECT AVG(year_result) AS [AVG], section_id
FROM student
GROUP BY section_id
HAVING AVG(year_result) =
	(SELECT	
		MAX(MOYENNE)
	FROM
		(SELECT 
			AVG(year_result) AS MOYENNE
		FROM student
		GROUP BY section_id) AS TEMP)

-------------------------------------

SELECT TOP 1
	AVG(year_result), 
	section_id
FROM student
GROUP BY section_id
ORDER BY AVG(year_result) DESC
