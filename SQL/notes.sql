USE Demo;


SELECT * FROM student; -- selectionnner toutes les colonnes
----------------------------------------
SELECT
	first_name,
	last_name,
	birth_date

FROM student;
-----------------------------------------
SELECT last_name AS 'Nom de famille' FROM student; -- Aliaser une colonne (simple ' si espace) - AS facultatif.
-----------------------------------------
SELECT last_name, 42 FROM student; -- Selection d'info non pr�sentes dans la table
-----------------------------------------
--Effectuer un calcul sur une colonne,

SELECT 
	last_name + ' ' + first_name AS FULLNAME, -- concat�nation + on peut concatener un INT en parsant avant via CONVERT
	year_result * 5 AS 'R�sultat sur 100',	-- multiplication de la colonne result
	(CONVERT(FLOAT,year_result)/20)*100 --> parse year_result INT en FLOAT pour realiser l'op�ration
FROM student


SELECT DISTINCT first_name, year_result --> recuperer seulement un element si doublon
	FROM student


------------------------------------------------------------------------------------------------
-- EXO
SELECT last_name, first_name AS Fname
FROM student

SELECT last_name AS lname, first_name AS fname
FROM student

SELECT last_name+'_'+first_name AS [name]
FROM student

SELECT 
	last_name+' '+first_name AS [name],
	year_result * 10 AS result
FROM student

------------------------------------------------------------------------------------------------

SELECT
	last_name, birth_date, [login],year_result
FROM student

------------------------------------------------------------------------------------------------

SELECT
	last_name+' '+first_name, student_id, birth_date
FROM student
--------------------------------------------------------------------------------------------------

SELECT
	(CONVERT(VARCHAR,student_id)) +' | '+ last_name + first_name+' | '+(CONVERT(VARCHAR,birth_date))+' | '+ [login]+' | '+(CONVERT(VARCHAR,year_result))+' | '+course_id
FROM student


SELECT *
FROM student
WHERE year_result = 18         
	OR last_name LIKE '_s%'; -- deuxi�me lettre s


-- BETWEEN : entre
-- IN : resultat est


--tri : ASC ou DESC
---------------------------------------------------------------------------------------------------
SELECT
	[login], year_result
FROM student
WHERE year_result > 16

----

SELECT
	last_name, section_id
FROM student
WHERE first_name = 'Georges'

----

SELECT
	last_name, year_result
FROM student
WHERE year_result BETWEEN 12 AND 16

----

SELECT
	last_name, section_id, year_result
FROM student
WHERE section_id NOT IN(1010, 1020, 1110)

----

SELECT
	last_name, section_id
FROM student
WHERE last_name like'%r'
----

SELECT
	last_name, year_result
FROM student
WHERE last_name like '__n%'
	AND year_result > 10

---

SELECT
	last_name, year_result
FROM student
WHERE year_result <= 3 
ORDER BY year_result DESC

---

SELECT
	last_name+' '+first_name, year_result
FROM student
WHERE section_id = '1010'
ORDER BY last_name ASC
---

SELECT
	last_name, section_id, year_result
FROM student
WHERE  section_id IN (1010,1020)
	AND year_result NOT BETWEEN 12 AND 18
ORDER BY section_id ASC
---

SELECT
	last_name, section_id, (year_result * 5) AS 'Resultat sur 100'
FROM student
WHERE section_id LIKE ('13%')
	AND (year_result*5) <= 60
ORDER BY 'Resultat sur 100' DESC
---

SELECT


FROM student










