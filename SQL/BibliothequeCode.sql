CREATE DATABASE Bibliotheque;
GO 
USE Bibliotheque;

CREATE TABLE Livre(
	ISBN CHAR(13) PRIMARY KEY,
	Titre NVARCHAR(100) NOT NULL,
	DateAchat DATETIME2 NOT NULL,
)

CREATE TABLE Client(
	Id INT IDENTITY PRIMARY KEY, --id auto incr�ment�
	SSN CHAR(11) NOT NULL UNIQUE, -- Unique = pas possible d'avoir 2 client avec le meme
	Nom NVARCHAR(50) NOT NULL,
	Rue NVARCHAR(255) NOT NULL,
	Numero NCHAR(10) NOT NULL,
	CodePostal CHAR(4) NOT NULL,
	Ville NVARCHAR(25) NOT NULL
)

CREATE TABLE Prenom(
	ClientID INT REFERENCES Client, -- reference vers id de la table client
	Prenom NVARCHAR(50) NOT NULL,
	CONSTRAINT PK_PRENOM PRIMARY KEY(ClientID,Prenom), -- Contrainte d'avoir une primary key combin� avec CLientID et prenom
)

CREATE TABLE Auteur(
	ISBN CHAR(13) REFERENCES Livre NOT NULL,
	Nom NVARCHAR(50) NOT NULL,
	CONSTRAINT PK_AUTEUR PRIMARY KEY(ISBN,Nom),
)

CREATE TABLE Emprunt(
	Numero CHAR(10) PRIMARY KEY,
	DateEmprunt DATETIME2 NOT NULL,
	DateRetour DATETIME2, 
		-- CHECK (DATEDIFF(d,DateRetour, DateEmprunt) <= 15), -- Contrainte verifier que la diff�rence entre DateEmprunt et retour est inf�rieur a 15J
	ISBN CHAR(13) REFERENCES Livre NOT NULL,
	ClientId INT REFERENCES Client NOT NULL,
)
