select 
    C.course_name,
    S.section_name,
    P.professor_name
from course as C
    inner join professor as P
        on C.professor_id = P.professor_id
    inner join section as S
        on S.section_id = P.section_id

-------------------------------------------

SELECT
	S.section_id,
	S.section_name,
	ST.last_name
FROM section AS S
	inner join student AS ST
		on S.delegate_id = ST.student_id

ORDER BY section_id DESC

---------------------------------------------

SELECT
	S.section_id,
	S.section_name,
	P.professor_name
FROM section AS S
	left join professor AS P
		ON S.section_id = P.section_id


----------------------------------------------

SELECT
	S.section_id,
	S.section_name,
	P.professor_name
FROM section AS S
	right join professor AS P
		ON S.section_id = P.section_id

---------------------------------------------

SELECT
	S.last_name,
	S.year_result,
	G.grade
FROM student AS S
	join grade AS G
		ON S.year_result BETWEEN G.lower_bound AND G.upper_bound
WHERE year_result >= 12
ORDER BY G.grade
	
-----------------------------------------------

SELECT
	P.professor_name,
	S.section_name,
	C.course_name,
	C.course_ects
FROM professor AS P
	join section AS S
		ON P.section_id = S.section_id
	left join course AS C
		ON P.professor_id = C.professor_id
ORDER BY C.course_ects DESC

-----------------------------------------------

SELECT
	P.professor_id,
	SUM(C.course_ects) AS ECTS_TOT
FROM professor AS P
	left join course AS C
		ON P.professor_id = C.professor_id
GROUP BY P.professor_id
ORDER BY SUM(C.course_ects) DESC

----------------------------------------------

SELECT first_name, last_name, 'S' AS categorie
FROM student
WHERE LEN(last_name) > 8
UNION
SELECT professor_surname, professor_name, 'P'
FROM professor
WHERE LEN(professor_name) > 8

--------------------------------------------

SELECT 
	S.section_id
FROM section AS S
	join professor AS P
		ON S.section_id = P.section_id
GROUP BY S.section_id
HAVING COUNT(P.section_id) = 0

------------------------------------------

SELECT 
	section_id 
FROM section
EXCEPT
SELECT
	section_id
FROM professor
