SELECT
	AVG(year_result) AS moyenne
FROM student

SELECT
	MAX(year_result) AS [max]
FROM student

SELECT
	SUM(year_result) AS somme
FROM student

SELECT
	MIN(year_result) AS [min]
FROM student

SELECT
	COUNT (*) AS compte
FROM student

SELECT
	[login], YEAR(birth_date)
FROM student
WHERE YEAR(birth_date) > 1970

SELECT
	[login], last_name
FROM student
WHERE LEN(last_name) >= 8

SELECT
	UPPER(last_name) AS 'Nom de famille', first_name
FROM student
WHERE year_result >= 16
ORDER BY year_result DESC

SELECT
	last_name, first_name, [login], SUBSTRING(first_name,1,2)+LOWER(SUBSTRING(last_name,1,4)) AS 'Nouveau login'
FROM student
WHERE year_result BETWEEN 6 AND 10

SELECT
	last_name, first_name, [login], SUBSTRING(first_name,(LEN(first_name)-2),3) + CONVERT(varchar,YEAR(GETDATE())- YEAR(birth_date)) AS 'Nouveau login'
FROM student
WHERE year_result IN(10,12,14)

SELECT
	last_name, [login], year_result
FROM student
WHERE SUBSTRING(last_name,1,1) IN('D','M','S')
ORDER BY birth_date

SELECT
	last_name, [login], year_result
FROM student
WHERE (year_result % 2 != 0)
	AND year_result > 10
ORDER BY year_result DESC
	

SELECT
	COUNT(*) AS '+ de 7 lettres'
FROM student
WHERE LEN(last_name)>=7

SELECT
	last_name, year_result,
CASE
	WHEN year_result >= 12 THEN 'OK'
	ELSE 'KO'
END AS [Note globale]	
FROM student
WHERE YEAR(birth_date) < 1955

SELECT
	last_name, year_result,
CASE
	WHEN year_result < 10 THEN 'Inferieur'
	WHEN year_result = 10 THEN 'Neutre'
	ELSE 'Superieur'
END AS 'Categorie'
FROM student
WHERE YEAR(birth_date) BETWEEN 1955 AND 1965


SELECT
	last_name, year_result, CONVERT(VARCHAR,birth_date,103) -- FORMAT(birth_date, 'dd MMMM yy')
FROM student
WHERE YEAR(birth_date) BETWEEN 1975 AND 1985


SELECT
	last_name,
	MONTH(birth_date),
	NULLIF(year_result, 4)
FROM student
WHERE MONTH(birth_date) NOT IN(12,1,2,3)
	AND year_result < 7

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

SELECT
	section_id,
	MAX(year_result) AS 'Resultat maximum'
FROM student
GROUP BY section_id

------------------------------------------

SELECT
	section_id,
	AVG(CONVERT(FLOAT,year_result)) AS Moyenne
FROM student
WHERE convert(varchar,section_id) like '10%'
GROUP BY section_id

------------------------------------------

SELECT
	MONTH(birth_date) AS 'Mois de naissance',
	AVG(year_result) AS Moyenne
FROM student
WHERE YEAR(birth_date) BETWEEN 1970 AND 1985
GROUP BY MONTH(birth_date)

------------------------------------------

SELECT
	section_id,
	AVG(CONVERT(FLOAT,year_result)) AS Moyenne
FROM student
GROUP BY section_id
HAVING COUNT(section_id) > 3

----------------------------------------

SELECT
	section_id,
	AVG(year_result),
	MAX(year_result)
FROM student

GROUP BY section_id
HAVING AVG(year_result) > 8

------------------------------------------