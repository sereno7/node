﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exemple.Hubs
{
    public class TestHub : Hub
    {
        public static ConcurrentDictionary<string, string> ConnectedUsers = new ConcurrentDictionary<string, string>();
        public static List<string> users = new List<string>();

        public async Task send(string message)
        {
            string id = Context.ConnectionId;
            var pseudo = ConnectedUsers.FirstOrDefault(x => x.Key == id).Value;
            if (pseudo != null)
            {
                await Clients.All.SendAsync("Recevoir", message, pseudo);  //return 
                //await Clients.Caller.SendAsync("messageSend", true);
            }
            else
                await Clients.Caller.SendAsync("noRegistered", "Veuillez vous enregistrer");
        }

        public async Task register(string pseudo)
        {
            string UserConnectionId;
            if (!ConnectedUsers.TryGetValue(Context.ConnectionId, out UserConnectionId))
            {
                ConnectedUsers.TryAdd(Context.ConnectionId, pseudo);
                users.Add(pseudo);
                await Clients.All.SendAsync("listeUsers", users);
            }
            else
                await Clients.Caller.SendAsync("alreadyRegistered", "Vous êtes déjà enregistré");
        }

        //public async Task disconnected()
        //{
        //    var test = "";
        //    ConnectedUsers.TryRemove(Context.ConnectionId, out test);
        //    if (test != null)
        //        await Clients.All.SendAsync("listeUsers", users);
        //}


        //await Clients.All.SendAsync("getPseudo", message);  //return 
        //public async Task Confirmation()
        //{
        //    await Clients.Caller.SendAsync("Confirme", "Bien Reçu over!");
        //}

    }
}
