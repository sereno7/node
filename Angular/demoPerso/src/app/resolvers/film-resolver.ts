import { Film } from "../models/film";
import { Resolve, ActivatedRoute } from "@angular/router";
import { FilmService } from "../services/film.service";
import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
  })
  
export class FilmResolver implements Resolve<Film>{
    constructor(private filmService:FilmService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Film | import("rxjs").Observable<Film> | Promise<Film> {
        return this.filmService.getDetails(route.params.id);       
    }  
}