import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'imagesFolder'
})
export class ImagesFolderPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return environment.imageFolder + value;
  }

}
