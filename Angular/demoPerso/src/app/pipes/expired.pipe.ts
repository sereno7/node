import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'expired'
})
export class ExpiredPipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): boolean {
    return value < new Date();
  }

}
