import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.scss']
})
export class Exo1Component implements OnInit {

  time : any;
  nbr;
  on : boolean;
  constructor() { }

  ngOnInit(): void {
    this.nbr = 0;
  }

  start(){
  this.time = setInterval(() =>{
      this.nbr++;
    },1);
    this.on = true;
  }

  stop(){
    clearInterval(this.time);
    this.on = false;
  }

  reset(){
    this.stop();
    this.nbr = 0;
  }

}
