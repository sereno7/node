import { Component, OnInit } from '@angular/core';
import { Article } from '../../../models/article';
import { NbDialogService, NbDialogRef } from '@nebular/theme';


@Component({
  selector: 'app-exo2',
  templateUrl: './exo2.component.html',
  styleUrls: ['./exo2.component.scss']
})

export class Exo2Component implements OnInit {
  items:Article[];
  maVariable:string;
  constructor() { }

  ngOnInit(): void {
    this.items = [];
    } 


  onClick(){
    //let foundedItem = this.items.find(a => a.name == this.maVariable); // retrouver une valeur et la stocker
    let b=false;
    this.items.forEach(element => {
      if(element.name.toLowerCase().trim() == this.maVariable.toLowerCase().trim())
      {
        element.quantity++;
        b=true;
      }
    })
    if(b==false)
      this.items.push({name: this.maVariable,isChecked: false, quantity: 1});
    this.maVariable = null;
  }

  delete(article: Article){    
    let r = confirm("êtes vous sur de vouloir supprimer l'article "+ article.name + " ?")
    if(r)
      if(article.quantity > 1)
        article.quantity--;
      else
        this.items=this.items.filter(a => a != article);

    //Première solution
    // let i = this.items.indexOf(article);
    // this.items.splice(i,1);
    //Deuxième solution
    // this.items=this.items.filter(a => a != article);
  }

  clickOnName(Article){
    if(Article.isChecked == false)
    {
      Article.isChecked = true;
    }
    else
    Article.isChecked = false
    }
}
