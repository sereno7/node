import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PokeResults } from '../../../models/poke-results';
import { pokemon } from '../../../models/pokemon';

@Component({
  selector: 'app-exo3',
  templateUrl: './exo3.component.html',
  styleUrls: ['./exo3.component.scss']
})


export class Exo3Component implements OnInit {
  urlApi = 'https://pokeapi.co/api/v2/pokemon/';  // URL de l'API
  tab: any[];
  tabPokemon: pokemon;
  visible : boolean;
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.visible = false;
    this.http.get<PokeResults>(this.urlApi)
      .subscribe(data => {
        this.tab = data.results;
      })
  }

  clickOnPokemon(url){
    this.visible = true;
    this.http.get<pokemon>(url)
      .subscribe(data => {
        this.tabPokemon = data;
      })
  }

}
