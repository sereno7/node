import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {
  visible  : boolean;
  items:string[];
  constructor() { }

  ngOnInit(): void {
    this.visible = false;
    this.items = ["sel","poivre","sucre"]
  }

  toggleVisible(){
    this.visible = !this.visible;
  }
}
