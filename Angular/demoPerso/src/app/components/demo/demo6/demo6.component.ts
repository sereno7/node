import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Film } from '../../../models/film';
import { FilmService } from '../../../services/film.service';

@Component({
  selector: 'app-demo6',
  templateUrl: './demo6.component.html',
  styleUrls: ['./demo6.component.scss']
})
export class Demo6Component implements OnInit {

  films: Film[];
  selectedFilm:Film;

  constructor(
    private filmService: FilmService
  ) { }

  ngOnInit(): void {
    this.filmService.context
      .subscribe(data => this.films = data);
  }

  getDetails(f: Film){
    this.selectedFilm = f;
  }
}
