import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilmService } from '../../../services/film.service';
import { Film } from '../../../models/film';

@Component({
  selector: 'app-demo6-details',
  templateUrl: './demo6-details.component.html',
  styleUrls: ['./demo6-details.component.scss']
})
export class Demo6DetailsComponent implements OnInit {

  film: Film;
  constructor(
    private activatedRoute: ActivatedRoute, 
    private filmService: FilmService
    ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data =>{
      this.film = data.film
    });
    
    // this.film = this.activatedRoute.snapshot.params['id'];
    // this.activatedRoute.params.subscribe(p =>{
    //   this.filmService.getDetails(p.id)
    //     .subscribe(data => this.film = data)
    // })
  } 
}
