import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  links: NbMenuItem[];
  ngOnInit(){
    this.links = [
      {link: '/home', title: 'Home', icon: 'home'},
      {link: '/about', title: 'About', icon: 'star'},
      {link: 'Demo', title: 'book', children: [
        { link: '/demo/demo1', title: 'Demo1 - Binding'},
        { link: '/demo/demo2', title: 'Demo2 - Event'},
        { link: '/demo/demo3', title: 'Demo3 - structural directives'},
        { link: '/demo/demo4', title: 'Demo4 - Binding 2 way'},
        { link: '/demo/demo5', title: 'Demo5 - model'},
        { link: '/demo/demo6', title: 'Demo6 - API'},
      ]},
      {link: 'Exo', title: 'Exercice', icon: 'award-outline', children: [
        { link: '/exo/exo1', title: 'Exo1 - Timer'},
        { link: '/exo/exo2', title: 'Exo2 - CheckList'},
        { link: '/exo/exo3', title: 'Exo3 - API'}
    ]},
  ]};
}
