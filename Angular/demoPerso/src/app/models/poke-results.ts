export interface PokeResults {
    count: number;
    next: string;
    previous: string;
    results: any[]
}