export interface Product{
    name: string;
    price: number;
    expirationDate: Date;
    image: string;
}