import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Film } from '../models/film';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  context: BehaviorSubject<Film[]>;

  private readonly GHIBLI_API: string 
  = "https://ghibliapi.herokuapp.com/films/";

  constructor(
    private client: HttpClient
  ) {
    this.context= new BehaviorSubject<Film[]>(null);
    this.refresh();
   }

  refresh(){
    this.client.get<Film[]>(this.GHIBLI_API)
      .subscribe(data =>{
        this.context.next(data);
      });
  }

  getDetails(id:string) : Observable<Film>{
   return this.client.get<Film>(this.GHIBLI_API + id)
  }
}
