import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Marvel } from '../../../models/marvel';
import { Film } from '../../../models/film';
import { FilmService } from '../../../../services/film.service';

@Component({
  selector: 'app-marvel-details',
  templateUrl: './marvel-details.component.html',
  styleUrls: ['./marvel-details.component.scss']
})
export class MarvelDetailsComponent implements OnInit {
  film: Marvel;
  selectedFilm: Film;
  constructor(
    private activatedRoute: ActivatedRoute,
    private filmService: FilmService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data =>{
      let id = data.id
      this.filmService.getDetails(id).subscribe(p =>{
        this.film = p;
      })
    })
  }

  showDetails(url: string):void{
    this.filmService.getFilmDetails(url).subscribe(data =>{
      this.selectedFilm = data;
    })

  }
}
