import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilmService } from '../../../../../services/film.service';
import { Marvel } from '../../../../models/marvel';
import { Film } from '../../../../models/film';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit {

  @Input() film:Film;
  constructor(
    // private activatedRoute: ActivatedRoute,
    // private filmService: FilmService,
  ) { }

  ngOnInit(): void {
    // this.activatedRoute.params.subscribe(data =>{
    //   let id = data.id;
    //   this.filmService.getFilmDetails(id).subscribe(p =>{
    //     this.film = p;
    //   })
    // })
  }
}
