import { Component, OnInit } from '@angular/core';
import { FilmService } from '../../../services/film.service';
import { Marvel } from '../../models/marvel';

@Component({
  selector: 'app-marvel',
  templateUrl: './marvel.component.html',
  styleUrls: ['./marvel.component.scss']
})
export class MarvelComponent implements OnInit {
  films: Marvel;
  constructor(
    private filmService: FilmService
  ) { }

  ngOnInit(): void {
    this.filmService.context
      .subscribe(data => this.films = data);
  }
}
