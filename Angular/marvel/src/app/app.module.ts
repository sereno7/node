import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbButtonModule, NbIconModule, NbMenuModule, NbSidebarModule, NbCardModule, NbInputModule, NbSelectModule, NbListModule, NbDialogModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ExercicesComponent } from './components/exercices/exercices.component';
import { MarvelComponent } from './components/exercices/marvel/marvel.component';
import { ImgMarvelPipe } from './pipes/img-marvel.pipe';
import { MarvelDetailsComponent } from './components/exercices/marvel/marvel-details/marvel-details.component';
import { FilmDetailsComponent } from './components/exercices/marvel/marvel-details/film-details/film-details.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    MarvelComponent,
    ExercicesComponent,
    ImgMarvelPipe,
    MarvelDetailsComponent,
    FilmDetailsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'corporate' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbMenuModule.forRoot(),
    NbIconModule,
    NbSidebarModule.forRoot(),
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    FormsModule, //pour le binding 2 ways
    NbListModule,
    NbDialogModule.forRoot(),
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
