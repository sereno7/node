import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Md5 } from 'ts-md5/dist/md5';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Marvel } from '../components/models/marvel';
import { Film } from '../components/models/film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  context: BehaviorSubject<Marvel>;
  md5:Md5 = new Md5();
  private key_public = "ee9a6ae0717eb43de199316992f615e4";
  private key_private = "0c85c214a0041e7844ee447e4e3f6ced31f684b0";
  private ts: Number = Date.now();
  hash = this.md5.appendStr(this.ts+this.key_private+this.key_public).end();

  private readonly MARVEL_API: string 
  = "https://gateway.marvel.com/v1/public/characters?ts="+this.ts+"&apikey="+this.key_public+"&hash="+this.hash;

  private readonly MARVEL_API_DETAILS: string 
  = "https://gateway.marvel.com/v1/public/characters/";

  private readonly MARVEL_API_FILM_DETAILS: string 
  = "https://gateway.marvel.com/v1/public/comics/";

  private readonly MARVEL_API_HASH: string 
  = "?ts="+this.ts+"&apikey="+this.key_public+"&hash="+this.hash;


  constructor(
    private client: HttpClient,   
  ) {
    this.context= new BehaviorSubject<Marvel>(null); 
    this.refresh();
   }

  refresh(){
    this.client.get<Marvel>(this.MARVEL_API)
      .subscribe(data =>{
        this.context.next(data);
      });
  }
  getDetails(id:string) : Observable<Marvel>{
    return this.client.get<Marvel>(this.MARVEL_API_DETAILS + id + this.MARVEL_API_HASH)
   }

   getFilmDetails(url:string) : Observable<Film>{
    return this.client.get<Film>(url + this.MARVEL_API_HASH)
   }

}
