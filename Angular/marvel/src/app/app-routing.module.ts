import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ExercicesComponent } from './components/exercices/exercices.component';
import { MarvelComponent } from './components/exercices/marvel/marvel.component';
import { MarvelDetailsComponent } from './components/exercices/marvel/marvel-details/marvel-details.component';
import { FilmDetailsComponent } from './components/exercices/marvel/marvel-details/film-details/film-details.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent},
      { path: 'about', component: AboutComponent},
      { path: 'exercices', component: ExercicesComponent, children:[
        {path: 'marvel', component: MarvelComponent},
        {path: 'marvelDetails/:id', component: MarvelDetailsComponent, children:[
          {path: 'filmDetails/:id', component: FilmDetailsComponent},
        ]},

        // {
        //   path: 'demo6-details/:id', 
        //   component: Demo6DetailsComponent,
        //   resolve: { film: FilmResolver}
        // }
      ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
