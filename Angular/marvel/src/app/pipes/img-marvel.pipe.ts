import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imgMarvel'
})
export class ImgMarvelPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return value+"/portrait_small.jpg";
  }

}
