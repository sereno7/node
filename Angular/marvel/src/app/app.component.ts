import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  links: NbMenuItem[];
  ngOnInit(){
    this.links = [
      {link: '/home', title: 'Home', icon: 'home'},
      {link: '/about', title: 'About', icon: 'star'},
      {link: 'Exercices', title: 'Exercice', icon: 'award-outline', children: [
        { link: '/exercices/marvel', title: 'Marvel'},
    ]},
  ]};
}
