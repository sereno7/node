// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  MARVEL_BASE_URL: "https://gateway.marvel.com/v1/public/",
  MARVEL_PUB_KEY: "ae66bcbc1050a95733dc0de639d3d515",
  MARVEL_PRI_KEY: "382f159e0c2c541f9b8d2c22fab328ea7de958b5",
  MOVIE_BASE_URL: "http://khunly.somee.com/api/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
