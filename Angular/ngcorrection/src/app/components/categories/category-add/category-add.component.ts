import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from 'src/app/services/categories.service';
import { Category } from 'src/app/models/category';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.scss']
})
export class CategoryAddComponent implements OnInit {

  fg: FormGroup;

  constructor(
    private categoriesService: CategoriesService,
    private toastr: NbToastrService
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Description' : new FormControl(null, Validators.compose([

      ])),
    });
  }

  submit() {
    if(this.fg.valid) {
      this.categoriesService.add(this.fg.value)
        .subscribe(data => {
          this.toastr.success('cool');
        }, error => {
          this.toastr.danger('zut');
        });
    }
  }

}
