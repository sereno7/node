import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbButtonModule, NbDialogModule, NbCardModule, NbListModule, NbInputModule, NbToastrModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { CharactersComponent } from './components/characters/characters.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MarvelImagePipe } from './pipes/marvel-image.pipe';
import { CharactersDetailsComponent } from './components/characters-details/characters-details.component';
import { MarvelInterceptor } from './interceptors/marvel.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { LoaderComponent } from './components/loader/loader.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoryAddComponent } from './components/categories/category-add/category-add.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CharactersComponent,
    MarvelImagePipe,
    CharactersDetailsComponent,
    LoaderComponent,
    CategoriesComponent,
    CategoryAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbButtonModule,
    NbDialogModule.forRoot(),
    NbCardModule,
    NbListModule,
    HttpClientModule,
    ReactiveFormsModule,
    NbInputModule,
    NbToastrModule.forRoot(),
  ],
  providers: [ 
    { provide: HTTP_INTERCEPTORS, useClass: MarvelInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
