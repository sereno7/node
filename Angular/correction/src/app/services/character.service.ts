import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Characters } from '../models/characters';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  context: BehaviorSubject<Characters>

  constructor(private http: HttpClient) { 
    this.context = new BehaviorSubject<Characters>(null);
    this.refreshContext();
   }

  refreshContext(): void{
      this.http.get<Characters>(environment.MARVEL_BASE_URL+'characters?offset=100')
      .subscribe(data =>{
        this.context.next(data);
      })
  }

  getDetails(id:number): Observable<Characters>{
    return this.http.get<Characters>(environment.MARVEL_BASE_URL+'characters/' + id)

  }

  // getCredentials(): string{
  //   let md5 = new Md5();
  //   let ts = new Date().getTime();;
  //   let hash = md5.appendStr(ts+environment.MARVEL_PRI_KEY+environment.MARVEL_PUB_KEY).end();
  //   return `?ts=${ts}&apikey=${environment.MARVEL_PUB_KEY}&hash=${hash}`;
  // }
}
