import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Category } from '../models/category';
import { runInThisContext } from 'vm';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  context: BehaviorSubject<Category[]>
  private url: string = environment.MOVIE_BASE_URL + "Category/";
  constructor(
    private http: HttpClient) 
    { 
      this.context = new BehaviorSubject<Category[]>(null);
      this.refresh();
    }


  refresh(){
    this.http.get<Category[]>(this.url).subscribe(data =>{
      this.context.next(data)
    })
  }

  add(model: Category){
    return this.http.post(this.url, model).pipe(finalize(() =>{
      this.refresh();

    }));
  }

  delete(id:number){
    return this.http.delete(this.url + id).pipe(finalize(() => {
      this.refresh();
    }));
  }

  update(model: Category){
    return this.http.put(this.url, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  getDetails(id: number){
    this.http.get<Category>(this.url + id)
  }
}

