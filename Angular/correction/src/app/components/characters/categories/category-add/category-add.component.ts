import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from '../../../../services/categories.service';
import { NbToastrService} from '@nebular/theme'

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.scss']
})
export class CategoryAddComponent implements OnInit {

  fg:FormGroup;
  constructor(
    private categoriesService: CategoriesService,
    private toastr: NbToastrService,
    ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(255),

      ])), //params : valeur par default, validators ajouter au champs name
      'Description': new FormControl(null, Validators.compose([

      ])),
    });
  }

  submit(){
    if(this.fg.valid)
    {
      this.categoriesService.add(this.fg.value)
        .subscribe(data => {
          this.toastr.success('Cool ca marche!');
        }, error => {
          this.toastr.danger('fuck ça ne fonctionne pas!!');
        });
    }
  }
}

      //envoi fg.value -> un objet qui respect le model category, c'est mapper automatiquement
      //subscribe avec une methode data quand ok 
      //error lorsqu'il y a une erreur

