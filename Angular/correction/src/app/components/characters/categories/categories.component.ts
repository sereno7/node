import { Component, OnInit } from '@angular/core';

import { CategoriesService } from '../../../services/categories.service';
import { Category } from '../../../models/category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  model:Category[];
  constructor(
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.categoriesService.context.subscribe(data =>{
      this.model = data;
    })
  }

  delete(id: number){
    this.categoriesService.delete(id).subscribe();

  }

}
