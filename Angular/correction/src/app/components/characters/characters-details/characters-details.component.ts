import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../../../services/character.service';
import { ActivatedRoute } from '@angular/router';
import { Characters } from '../../../models/characters';

@Component({
  selector: 'app-characters-details',
  templateUrl: './characters-details.component.html',
  styleUrls: ['./characters-details.component.scss']
})
export class CharactersDetailsComponent implements OnInit {
  model:Characters;
  constructor(private characterService: CharacterService,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe(p=>{
      this.characterService.getDetails(p.id)
      .subscribe(data => {
        this.model = data;
    })
    
      })
  }

}
