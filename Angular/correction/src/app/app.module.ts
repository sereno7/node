import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbButtonModule, NbCardModule, NbListModule, NbInputModule, NbDialogModule, NbToastrModule, NbIconModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { CharactersComponent } from './components/characters/characters.component';
import { CharactersDetailsComponent } from './components/characters/characters-details/characters-details.component';
import { MarvelInterceptor } from './interceptors/marvel.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { LoaderComponent } from './components/characters/loader/loader.component';
import { CategoriesComponent } from './components/characters/categories/categories.component';
import { CategoryAddComponent } from './components/characters/categories/category-add/category-add.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CharactersComponent,
    CharactersDetailsComponent,
    LoaderComponent,
    CategoriesComponent,
    CategoryAddComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbButtonModule,
    NbDialogModule.forRoot(),
    NbCardModule,
    NbListModule,
    NbInputModule,
    HttpClientModule,
    ReactiveFormsModule,
    NbToastrModule.forRoot(),
    NbIconModule
    
  ],
  providers: [
    
    { provide: HTTP_INTERCEPTORS, useClass: MarvelInterceptor, multi:true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi:true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
