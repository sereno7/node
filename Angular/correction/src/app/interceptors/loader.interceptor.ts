import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize, delay } from 'rxjs/operators';
import { NbDialogService } from '@nebular/theme';
import { LoaderComponent } from '../components/characters/loader/loader.component';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private dialogServices: NbDialogService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let box = this.dialogServices.open(LoaderComponent);
    return next.handle(request).pipe(finalize(() =>{
      //faire quelque chose à la fin de la requete
      //cacher le loader
      // delay(1000);
      setTimeout(() =>{
        box.close();
      },500)    
    }))
  }
}
