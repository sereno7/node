import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Md5 } from 'ts-md5';
import { environment } from '../../environments/environment';

@Injectable()
export class MarvelInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(request.url.includes(environment.MARVEL_BASE_URL)){  // si inclu url base marvel ajoute credentials sinon request normal
      let clone = this.addCredentials(request);
      return next.handle(clone); //envoyer le clone
    }
    return next.handle(request);
    
  }

  addCredentials(request: HttpRequest<unknown>) : HttpRequest<unknown>{
    let md5 = new Md5();
    let ts = new Date().getTime();;
    let hash = md5.appendStr(ts+environment.MARVEL_PRI_KEY+environment.MARVEL_PUB_KEY).end();
    let clone = request.clone({params: request.params.set('ts', ts.toString())});
    clone = clone.clone({params: clone.params.set('apikey', environment.MARVEL_PUB_KEY)});
    clone = clone.clone({params: clone.params.set('hash', hash.toString()) });
    return clone;
  }
}
