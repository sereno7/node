// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  MARVEL_BASE_URL:"https://gateway.marvel.com/v1/public/",
  MARVEL_PUB_KEY:"ee9a6ae0717eb43de199316992f615e4",
  MARVEL_PRI_KEY:"0c85c214a0041e7844ee447e4e3f6ced31f684b0",
  MOVIE_BASE_URL:"http://khunly.somee.com/api/",

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
