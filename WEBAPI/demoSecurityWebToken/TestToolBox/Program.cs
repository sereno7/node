﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.JWT;

namespace TestToolBox
{
    class Program
    {
        class User
        {
            public int Id { get; set; }
            public string UserName { get; set; }
            public string Role { get; set; }
        }
        static void Main(string[] args)
        {
            TokenService s = new TokenService();
            string t=
                s.Encode(new User { Id = 42, UserName = "Khun", Role = "ADMIN" });
            Console.WriteLine(t);

            if(s.Decode("eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjQyIiwiVXNlck5hbWUiOiJLaHVuIiwiUm9sZSI6IkFETUlOIiwibmJmIjoxNTk4NjA5MTA5LCJleHAiOjE1OTg2MDkxMTAsImlzcyI6IlRlc3RUb29sQm94IiwiYXVkIjoiTm9ib2R5In0.Y2eqMIMZ6b7ndbnp9qAppOKDW3FdR6wZixGQnMRWdutziki4e3K9SPxVmY585-108yxz5FpOWyZGpRMv5rDWKg") != null)
            {
                Console.WriteLine("le token est valide");
            }
            else
            {
                Console.WriteLine("le token est invalide");
            }

            Console.ReadKey();
        }
    }
}
