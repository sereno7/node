﻿using ExerciceAPI.Models;
using ExerciceAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExerciceAPI.Controllers
{
    public class MovieController : ApiController
    {
        private MoviesService _movieService;
        public MovieController()
        {
            _movieService = new MoviesService();
        }
        [HttpGet]
        public List<Movies> Get()
        {
            List<Movies> movies = _movieService.GetAll().ToList();

            ActorsMoviesService ams = new ActorsMoviesService();


            for (int i = 0; i < movies.Count(); i++)
            {
                IEnumerable<Actors> actors = ams.GetActorsByMovieId(movies[i].Id);
                movies[i].MyActors = actors.ToList();
            }

            return movies;
        }
        [HttpGet]
        public Movies Get(int id)
        {
            ActorsMoviesService ams = new ActorsMoviesService();

            Movies m = _movieService.GetAll().FirstOrDefault(x => x.Id == id);

            m.MyActors = ams.GetActorsByMovieId(id);

            return m;
        }
        [HttpPost]
        public void Post(Movies m)
        {
            _movieService.Insert(m);
        }

        [HttpPut]
        public void Update(Movies m)
        {
            _movieService.Update(m);
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _movieService.Delete(id);
        }
    }
}
