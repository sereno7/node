﻿using ExerciceAPI.Models;
using ExerciceAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExerciceAPI.Controllers
{
    public class ActorController : ApiController
    {
        private ActorsService _actorService;
        public ActorController()
        {
            _actorService = new ActorsService();
        }
        [HttpGet]
        public List<Actors> Get()
        {
            List<Actors> actors = _actorService.GetAll().ToList();

            ActorsMoviesService ams = new ActorsMoviesService();


            for (int i = 0; i < actors.Count(); i++)
            {
                IEnumerable<Movies> movies = ams.GetMoviesByActorId(actors[i].Id);
                actors[i].MyMovies = movies.ToList();
            }

            return actors;
        }
        [HttpGet]
        public Actors Get(int id)
        {
            ActorsMoviesService ams = new ActorsMoviesService();

            Actors a = _actorService.GetAll().FirstOrDefault(x => x.Id == id);

            a.MyMovies = ams.GetMoviesByActorId(id);

            return a;
        }

        [HttpPost]
        public void Post(Actors a)
        {
            _actorService.Insert(a);
        }

        [HttpPut]
        public void Update(Actors a)
        {
            _actorService.Update(a);
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _actorService.Delete(id);
        }

        [HttpPost]
        [Route("api/actor/{IdMovie}/{IdActor}")]
        public void Insert(int IdMovie,int IdActor)
        {
            ActorsMoviesService ams = new ActorsMoviesService();

            ams.Insert(IdMovie, IdActor);
        }

        [HttpDelete]
        [Route("api/actor/{IdMovie}/{IdActor}")]
        public void Delete(int IdMovie, int IdActor)
        {
            ActorsMoviesService ams = new ActorsMoviesService();

            ams.Delete(IdMovie, IdActor);
        }
    }
}
