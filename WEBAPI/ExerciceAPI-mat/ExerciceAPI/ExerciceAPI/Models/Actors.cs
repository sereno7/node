﻿using ExerciceAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExerciceAPI.Models
{
    public class Actors
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDate { get; set; }
        public IEnumerable<Movies> MyMovies { get; set; }
    }
}