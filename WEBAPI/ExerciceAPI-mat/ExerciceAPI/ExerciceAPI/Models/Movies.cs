﻿using ExerciceAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExerciceAPI.Models
{
    public class Movies
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Realisator { get; set; }
        public int ReleaseDate { get; set; }
        public string Synopsis { get; set; }
        public IEnumerable<Actors> MyActors { get; set; }

    }
}