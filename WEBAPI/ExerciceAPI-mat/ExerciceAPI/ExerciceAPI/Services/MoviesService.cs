﻿using ExerciceAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ExerciceAPI.Services
{
    public class MoviesService
    {
        private SqlConnection _connection;
        private string connectionString;

        public MoviesService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }
        public IEnumerable<Movies> GetAll()
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Movies";
                    using (SqlDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            yield return new Movies
                            {
                                Id = (int)r["Id"],
                                Title = r["Title"].ToString(),
                                Genre = r["Genre"].ToString(),
                                Realisator = r["Realisator"].ToString(),
                                ReleaseDate = (int)r["ReleaseDate"],
                                Synopsis = r["Synopsis"].ToString()
                            };
                        }
                    }
                }
            }
        }
        public void Insert(Movies m)
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Movies (Title,Genre,Realisator,ReleaseDate,Synopsis) VALUES (@Title, @Genre, @Realisator,@ReleaseDate,@Synopsis)";
                    cmd.Parameters.AddWithValue("Title", m.Title);
                    cmd.Parameters.AddWithValue("Genre", m.Genre);
                    cmd.Parameters.AddWithValue("Realisator", m.Realisator);
                    cmd.Parameters.AddWithValue("ReleaseDate", m.ReleaseDate);
                    cmd.Parameters.AddWithValue("Synopsis", m.Synopsis);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM Movies WHERE ID = @Id";
                    cmd.Parameters.AddWithValue("Id", id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Update(Movies m)
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "UPDATE Movies SET Title = @Title, Genre = @Genre, Realisator = @Realisator, ReleaseDate = @ReleaseDate, Synopsis = @Synopsis WHERE Id = @Id";
                    cmd.Parameters.AddWithValue("Id", m.Id);
                    cmd.Parameters.AddWithValue("Title", m.Title);
                    cmd.Parameters.AddWithValue("Genre", m.Genre);
                    cmd.Parameters.AddWithValue("Realisator", m.Realisator);
                    cmd.Parameters.AddWithValue("ReleaseDate", m.ReleaseDate);
                    cmd.Parameters.AddWithValue("Synopsis", m.Synopsis);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}