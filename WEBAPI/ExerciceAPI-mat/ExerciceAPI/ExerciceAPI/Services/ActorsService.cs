﻿using ExerciceAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;

namespace ExerciceAPI.Services
{
    public class ActorsService
    {
        private SqlConnection _connection;
        private string connectionString;

        public ActorsService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }
        public IEnumerable<Actors> GetAll()
        {
            using (SqlConnection c = new SqlConnection(connectionString))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Actors";
                    using (SqlDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            yield return new Actors
                            {
                                Id = (int)r["Id"],
                                FirstName = r["FirstName"].ToString(),
                                LastName = r["LastName"].ToString(),
                                BirthDate = (DateTime)r["BirthDate"]
                            };
                        }
                    }
                }
            }
        }
        public void Insert(Actors a)
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Actors (FirstName,LastName,BirthDate) VALUES (@FirstName, @LastName, @BirthDate)";
                    cmd.Parameters.AddWithValue("FirstName", a.FirstName);
                    cmd.Parameters.AddWithValue("LastName", a.LastName);
                    cmd.Parameters.AddWithValue("BirthDate", a.BirthDate);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM Actors WHERE ID = @Id";
                    cmd.Parameters.AddWithValue("Id", id);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Update(Actors a)
        {
            using (SqlConnection c = _connection)
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    cmd.CommandText = "UPDATE Actors SET LastName = @LastName, FirstName = @FirstName, BirthDate = @BirthDate where Id = @Id";
                    cmd.Parameters.AddWithValue("FirstName", a.FirstName);
                    cmd.Parameters.AddWithValue("LastName", a.LastName);
                    cmd.Parameters.AddWithValue("BirthDate", a.BirthDate);
                    cmd.Parameters.AddWithValue("Id", a.Id);

                    cmd.ExecuteNonQuery();
                }
            }
        }


    }
}