﻿using ExerciceAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ExerciceAPI.Services
{
    public class ActorsMoviesService
    {
        private string connectionString;

        public ActorsMoviesService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            
        }

        public IEnumerable<Movies> GetMoviesByActorId(int id)
        {
            using (SqlConnection c = new SqlConnection(connectionString))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    string query;
                    query = "SELECT M.Id,M.Title,M.Genre,M.Realisator,M.ReleaseDate,M.Synopsis FROM Movies as M";
                    query += " INNER JOIN ActorsMovies as AC ON AC.PK_MOVIE = M.Id";
                    query += $" WHERE PK_Actor = {id}";
                    cmd.CommandText = query;
                    using (SqlDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            yield return new Movies
                            {
                                Id = (int)r["Id"],
                                Title = r["Title"].ToString(),
                                Genre = r["Genre"].ToString(),
                                Realisator = r["Realisator"].ToString(),
                                ReleaseDate = (int)r["ReleaseDate"],
                                Synopsis = r["Synopsis"].ToString()
                            };
                        }
                    }
                }
            }
        }
        public IEnumerable<Actors> GetActorsByMovieId(int id)
        {
            using (SqlConnection c = new SqlConnection(connectionString))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    string query;
                    query = "SELECT A.Id,A.LastName,A.FirstName,A.BirthDate FROM Actors as A";
                    query += " INNER JOIN ActorsMovies as AC ON AC.PK_Actor = A.Id";
                    query += $" WHERE PK_Movie = {id}";
                    cmd.CommandText = query;
                    using (SqlDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            yield return new Actors
                            {
                                Id = (int)r["Id"],
                                LastName = r["LastName"].ToString(),
                                FirstName = r["FirstName"].ToString(),
                                BirthDate = (DateTime)r["BirthDate"]
                            };
                        }
                    }
                }
            }
        }
        public void Insert(int IdMovie, int IdActor)
        {
            using (SqlConnection c = new SqlConnection(connectionString))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    string query;
                    query = "Insert INTO ActorsMovies (PK_Movie,PK_Actor) VALUES (@IdMovie,@IdActor)";
                    cmd.Parameters.AddWithValue("IdMovie", IdMovie);
                    cmd.Parameters.AddWithValue("IdActor", IdActor);
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int IdMovie, int IdActor)
        {
            using (SqlConnection c = new SqlConnection(connectionString))
            {
                c.Open();
                using (SqlCommand cmd = c.CreateCommand())
                {
                    string query;
                    query = "DELETE FROM ActorsMovies WHERE PK_Movie = @IdMovie AND PK_Actor = @IdActor";
                    cmd.Parameters.AddWithValue("IdMovie", IdMovie);
                    cmd.Parameters.AddWithValue("IdActor", IdActor);
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}