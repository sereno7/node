﻿using PetShop.API.Services;
using PetShop.API.UOW;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PetShop.API.Validators
{
    public class UniqueAnimalNameAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            UnitOfWork uow = (UnitOfWork)context.GetService(typeof(UnitOfWork));
            if (uow.Get<AnimalService>().GetAll().FirstOrDefault(a => a.Name.ToLower() == value.ToString().ToLower()) == null)
            {
                return null;
            }
            return new ValidationResult("This Name Already Exists");
        }
    }
}