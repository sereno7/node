﻿using PetShop.DAL.EF.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.Services
{
    public class PetStatusService : BaseService
    {
        public PetStatusService(PetShopContext context) : base(context)
        {
        }

        public int GetByStatusName(string name)
        {
            return _context.PetStatuses.FirstOrDefault(s => s.Name == name)?.Id ?? 0;
        }
    }
}
