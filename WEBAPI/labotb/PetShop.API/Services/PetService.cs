﻿using PetShop.API.Dto.Pet;
using PetShop.DAL.EF.Context;
using PetShop.DAL.EF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolBox.Mappers.Extensions;

namespace PetShop.API.Services
{
    public class PetService : BaseService
    {
        public PetService(PetShopContext context) : base(context)
        {
        }

        public bool Delete(int id)
        {
            Pet toDelete = _context.Pets
                .SingleOrDefault(u => u.Id == id);
            if (toDelete == null)
            {
                return false;
            }
            _context.Pets.Remove(toDelete);
            return true;
        }

        public PetDetailsDto Get(int id)
        {
            return _context.Pets.ToList()
                .Where(p => p.Id == id)
                .Select(p => {
                    PetDetailsDto result = p.MapTo<PetDetailsDto>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.Animal.Name;
                    result.PetStatusName = p?.PetStatus?.Name;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                }).FirstOrDefault();
        }

        public IEnumerable<PetDto> GetAllFreeByAnimal(int animalId)
        {
            return _context.Pets.ToList()
                .Where(p => p.Breed?.AnimalId == animalId && p.UserId == null)
                .Select(p => {
                    PetDto result = p.MapTo<PetDto>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.Animal.Name;
                    result.PetStatusName = p?.PetStatus?.Name;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
        }

        public IEnumerable<PetDto> GetAllByUserId(int id)
        {
            return _context.Pets.ToList()
                .Where(p => p.UserId == id)
                .Select(p => {
                    PetDto result = p.MapTo<PetDto>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.Animal.Name;
                    result.PetStatusName = p?.PetStatus?.Name;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
        }

        public PetInsertDto Insert(PetInsertDto dto)
        {
            Breed breed = _context.Breeds.FirstOrDefault(b => b.Id == dto.BreedId);
            Pet toInsert = dto.MapTo<Pet>();
            toInsert.Breed = breed;
            _context.Pets.Add(toInsert);
            return dto;
        }

        public bool Update(PetInsertDto dto)
        {
            Pet toUpdate = _context.Pets.FirstOrDefault(p => p.Id == dto.Id);
            if (toUpdate == null) return false;
            Breed breed = _context.Breeds.FirstOrDefault(b => b.Id == dto.BreedId);
            toUpdate.Breed = breed;
            dto.MapToInstance(toUpdate);
            return true;
        }

        public bool UpdateStatus(PetUpdateStatusDto dto)
        {
            Pet toUpdate = _context.Pets.FirstOrDefault(p => p.Id == dto.Id);
            if (toUpdate == null) return false;
            dto.MapToInstance(toUpdate);
            return true;
        }

        public IEnumerable<PetDto> GetAll()
        {
            return _context.Pets.ToList()
                .Select(p => {
                    PetDto result = p.MapTo<PetDto>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.Animal.Name;
                    result.PetStatusName = p?.PetStatus?.Name;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
        }
    }
}
