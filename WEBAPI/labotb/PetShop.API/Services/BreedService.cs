﻿using PetShop.API.Dto.Breed;
using PetShop.DAL.EF.Context;
using PetShop.DAL.EF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolBox.Mappers.Extensions;

namespace PetShop.API.Services
{
    public class BreedService : BaseService
    {
        public BreedService(PetShopContext context) : base(context)
        {
        }

        public bool Delete(int id)
        {
            Breed toDelete = _context.Breeds.FirstOrDefault(a => a.Id == id);
            if (toDelete == null)
            {
                return false;
            }
            _context.Breeds.Remove(toDelete);
            return true;
        }

        public IEnumerable<BreedDto> GetAll()
        {
            return _context.Breeds
                .ToList().Select(b => b.MapTo<BreedDto>());
        }

        public IEnumerable<BreedDto> GetAllByAnimal(int animalid)
        {
            return _context.Breeds.Where(b => b.AnimalId == animalid)
                .ToList().Select(b => b.MapTo<BreedDto>());
        }

        public BreedDto Insert(BreedDto dto)
        {
            var toInsert = dto.MapTo<Breed>();
            toInsert.Id = 0;
            _context.Breeds.Add(toInsert);

            return dto;
        }
    }
}
