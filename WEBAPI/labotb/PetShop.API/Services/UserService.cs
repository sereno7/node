﻿using Microsoft.EntityFrameworkCore;
using PetShop.API.Dto;
using PetShop.API.Dto.Pet;
using PetShop.API.Dto.User;
using PetShop.DAL.EF.Context;
using PetShop.DAL.EF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using ToolBox.Mappers.Extensions;

namespace PetShop.API.Services
{
    public class UserService : BaseService
    {
        public UserService(PetShopContext context) : base(context)
        {
        }

        public bool Delete(int key)
        {
            User toDelete = _context.Users
                .SingleOrDefault(u => u.Id == key);
            if (toDelete == null)
            {
                return false;
            }
            _context.Users.Remove(toDelete);
            return true;
        }

        public UserDto Get(int key)
        {
            User u = _context.Users
                .SingleOrDefault(u => u.Id == key);
            UserDto dto = u?.MapTo<UserDto>();
            if(dto != null) 
            {
                dto.RoleName = u.Role?.Name;
            }
            return dto;
        }

        public UserDetailsDto GetDetails(int key)
        {
            User u = _context.Users
                .SingleOrDefault(u => u.Id == key);
            UserDetailsDto dto = u?.MapTo<UserDetailsDto>();
            if (dto != null)
            {
                dto.RoleName = u.Role?.Name;
                dto.Pets = u.Pets.Select(p =>
                {
                    PetDto result = p.MapTo<PetDto>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.Animal.Name;
                    result.PetStatusName = p?.PetStatus?.Name;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
            }
            return dto;
        }

        public UserDto GetByCredentials(LoginDto login)
        {
            User u = _context.Users.FromSqlRaw(
                "dbo.SP_User_Select_By_Credentials @p0, @p1",
                login.Email,
                login.Password
            ).ToList().FirstOrDefault();
            UserDto dto = u?.MapTo<UserDto>();
            if (dto != null)
            {
                dto.RoleName = u.Role?.Name;
            }
            return dto;
        }

        public IEnumerable<UserDto> GetAll()
        {
            return _context.Users.ToList().Select(u => {
                UserDto dto = u.MapTo<UserDto>();
                dto.RoleName = u.Role?.Name;
                return dto;
            });
        }

        public UserDto Insert(RegisterDto entity)
        {
            User u = _context.Users.FromSqlRaw(
                "dbo.SP_User_Insert @p0, @p1, @p2, @p3, @p4, @p5",
                entity.Email,
                entity.Password,
                entity.LastName,
                entity.FirstName,
                (object)entity.BirthDate ?? DBNull.Value,
                _context.Roles.FirstOrDefault(r => r.Name == "CUSTOMER")?.Id
            ).ToList().FirstOrDefault();
            UserDto dto = u?.MapTo<UserDto>();
            if (dto != null)
            {
                dto.RoleName = u.Role?.Name;
            }
            return dto;
        }

        public bool Update(UserUpdateDto entity)
        {
            User toUpdate = _context.Users.FirstOrDefault(u => u.Id == entity.Id);
            if (toUpdate == null) return false;
            entity.MapToInstance(toUpdate);
            return true;
        }

        public UserDto GetByEmail(string email)
        {
            return _context.Users
                .SingleOrDefault(u => u.Email.ToLower() == email.ToLower())?.MapTo<UserDto>();
        }
    }
}
