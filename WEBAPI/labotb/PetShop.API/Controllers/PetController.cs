﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PetShop.API.Dto.Pet;
using PetShop.API.Filters;
using PetShop.API.Services;
using PetShop.API.UOW;
using ToolBox.Mappers.Extensions;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PetController : MasterController
    {
        public PetController(UnitOfWork uow) : base(uow)
        {
        }

        [HttpGet]
        [Route("{id}")]
        [Produces("application/json", Type = typeof(PetDetailsDto))]
        public IActionResult Get(int id)
        {
            PetDetailsDto toReturn = Uow.Get<PetService>().Get(id);
            if (toReturn == null)
            {
                return NotFound();
            }
            return Ok(toReturn);
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<PetDetailsDto>))]
        public IActionResult Get([FromQuery]int? animalId = null)
        {
            if (animalId is int id)
                return Ok(Uow.Get<PetService>().GetAllFreeByAnimal(id));
            else
                return Ok(Uow.Get<PetService>().GetAll());
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(PetInsertDto))]
        public IActionResult Post([FromBody] PetInsertDto dto)
        {
            PetInsertDto newPet = Uow.Get<PetService>().Insert(dto);
            Uow.Save();
            return Ok(newPet);
        }

        [HttpPut]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Put([FromBody] PetInsertDto dto)
        {
            if (Uow.Get<PetService>().Update(dto))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(int id)
        {
            if (Uow.Get<PetService>().Delete(id))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPatch]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = null)]
        public IActionResult Patch(PetUpdateStatusDto dto)
        {
            PetUpdateStatusDto old = Uow.Get<PetService>().Get(dto.Id).MapTo<PetUpdateStatusDto>();
            if(User.IsInRole("ADMIN"))
            {
                old.PetStatusId = dto.PetStatusId;
                old.UserId = dto.UserId;
                if(dto.PetStatusId == null)
                {
                    old.UserId = null;
                }
            }
            else
            {
                int soldId = Uow.Get<PetStatusService>().GetByStatusName("SOLD");
                if(old.UserId == null || (old.UserId == UserId && old.PetStatusId != soldId))
                {
                    old.PetStatusId = dto.PetStatusId;
                    old.UserId = dto.UserId;
                }
                else
                {
                    return BadRequest();
                }
            }
            if (Uow.Get<PetService>().UpdateStatus(old))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("Image/{id}")]
        [Produces("image/png")]
        public IActionResult GetImage(int id)
        {
            PetDetailsDto toReturn = Uow.Get<PetService>().Get(id);
            if (toReturn == null || toReturn.Image == null)
            {
                return NotFound();
            }
            return File(toReturn.Image, toReturn.ImageMimeType);
        }
    }

    
}
