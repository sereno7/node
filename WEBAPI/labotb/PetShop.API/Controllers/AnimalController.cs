﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PetShop.API.Dto.Animal;
using PetShop.API.Filters;
using PetShop.API.Services;
using PetShop.API.UOW;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : MasterController
    {
        public AnimalController(UnitOfWork uow) : base(uow)
        {
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(AnimalDto))]
        public IActionResult Post([FromBody] AnimalDto dto)
        {
            AnimalService repo = Uow.Get<AnimalService>();
            repo.Insert(dto);
            Uow.Save();
            dto.Id = Uow.Get<AnimalService>()
                .GetAll().FirstOrDefault(b => b.Name == dto.Name)?.Id ?? 0;
            return Ok(dto);
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<AnimalDto>))]
        public IActionResult Get()
        {
            return Ok(Uow.Get<AnimalService>().GetAll());
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(int id)
        {
            if (Uow.Get<AnimalService>().Delete(id))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }
    }
}
