﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Dto.Breed;
using PetShop.API.Dto.Error;
using PetShop.API.Filters;
using PetShop.API.Services;
using PetShop.API.UOW;
using System.Collections.Generic;
using System.Linq;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreedController : MasterController
    {
        public BreedController(UnitOfWork uow) : base(uow)
        {
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<BreedDto>))]
        public IActionResult Get([FromQuery]int? animalId = null)
        {
            if(animalId is int id)
            {
                return Ok(Uow.Get<BreedService>().GetAllByAnimal(id));
            }
            else
            {
                return Ok(Uow.Get<BreedService>().GetAll());
            }
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(BreedDto))]
        public IActionResult Post([FromBody] BreedDto dto)
        {
            AnimalService aRepo = Uow.Get<AnimalService>();
            if (aRepo.GetAll().FirstOrDefault(a => a.Id == dto.AnimalId) == null)
            {
                return BadRequest(new ErrorDto("Name", "Verifie tes données"));
            }
            IEnumerable<BreedDto> breeds = Uow.Get<BreedService>().GetAll();
            if (breeds.FirstOrDefault(b => b.Name == dto.Name && b.AnimalId == dto.AnimalId) != null)
            {
                return BadRequest(new ErrorDto("Name", "This name already exists"));
            }
            Uow.Get<BreedService>().Insert(dto);
            Uow.Save();
            dto.Id = Uow.Get<BreedService>()
                .GetAll().FirstOrDefault(b => b.Name == dto.Name && b.AnimalId == dto.AnimalId)?.Id ?? 0;
            return Ok(dto);
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(int id)
        {
            if (Uow.Get<BreedService>().Delete(id))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }
    }
}
