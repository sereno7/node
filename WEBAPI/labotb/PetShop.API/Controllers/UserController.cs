﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PetShop.API.Dto;
using PetShop.API.Dto.User;
using PetShop.API.Filters;
using PetShop.API.Services;
using PetShop.API.UOW;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : MasterController
    {
        public UserController(UnitOfWork uow) : base(uow)
        {

        }

        [HttpPost]
        [Produces("application/json", Type = typeof(RegisterDto))]
        public IActionResult Post([FromBody] RegisterDto user)
        {
            UserDto dto = Uow.Get<UserService>().Insert(user);
            Uow.Save();
            return Ok(dto);
        }

        [HttpGet]
        [Route("{id}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = typeof(UserDto))]
        public IActionResult Get(int id)
        {
            if (!User.IsInRole("ADMIN") && id != UserId)
            {
                return Unauthorized();
            }
            UserDetailsDto dto = Uow.Get<UserService>().GetDetails(id);
            if (dto == null) return NotFound();
            return Ok(dto);
        }

        [HttpGet]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(IEnumerable<UserDto>))]
        public IActionResult Get()
        {
            return Ok(Uow.Get<UserService>().GetAll());
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json")]
        public IActionResult Delete(int id)
        {
            if ((!User.IsInRole("ADMIN") && id != UserId) || Uow.Get<UserService>().Get(id).RoleName == "ADMIN")
            {
                return Unauthorized();
            }
            if (Uow.Get<UserService>().Delete(id))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPut]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json")]
        public IActionResult Put([FromBody] UserUpdateDto user)
        {
            if (!User.IsInRole("ADMIN") && user.Id != UserId)
            {
                return Unauthorized();
            }
            if (Uow.Get<UserService>().Update(user))
            {
                Uow.Save();
                return Ok();
            }
            return BadRequest();
        }
    }
}
