﻿using Newtonsoft.Json;
using PetShop.API.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.Dto.Animal
{
    public class AnimalDto
    {

        public int Id { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [MaxLength(50)]
        [Required]
        [UniqueAnimalName]
        public string Name { get; set; }
    }
}