﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace PetShop.API.Dto.Breed
{
    public class BreedDto
    {
        public int Id { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        public int AnimalId { get; set; }
    }
}
