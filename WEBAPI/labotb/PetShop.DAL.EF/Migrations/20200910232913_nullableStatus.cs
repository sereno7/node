﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PetShop.DAL.EF.Migrations
{
    public partial class nullableStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pet_PetStatus_PetStatusId",
                table: "Pet");

            migrationBuilder.AlterColumn<int>(
                name: "PetStatusId",
                table: "Pet",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Pet_PetStatus_PetStatusId",
                table: "Pet",
                column: "PetStatusId",
                principalTable: "PetStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pet_PetStatus_PetStatusId",
                table: "Pet");

            migrationBuilder.AlterColumn<int>(
                name: "PetStatusId",
                table: "Pet",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Pet_PetStatus_PetStatusId",
                table: "Pet",
                column: "PetStatusId",
                principalTable: "PetStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
