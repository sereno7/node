﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PetShop.Services.Abstractions.Jwt;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace ToolBox.Security.Jwt
{
    public class JwtService : IJwtService
    {
        private readonly SymmetricSecurityKey _SecurityKey;
        private readonly JwtSecurityTokenHandler _Handler;
        private readonly string _Issuer;
        private readonly string _Audience;

        public JwtService(JwtConfig config)
        {
            _Handler = new JwtSecurityTokenHandler();
            _SecurityKey
                = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.Signature));
            _Issuer = config.Issuer;
            _Audience = config.Audience;
        }

        public ClaimsPrincipal DecodeJwt(string jwt)
        {
            TokenValidationParameters validationParameters = GetValidationParameters();
            try
            {
                return _Handler.ValidateToken(jwt, validationParameters, out SecurityToken validatedToken);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string EncodeJwt(IPayload payload)
        {
            return EncodeJwt(payload, DateTime.UtcNow.AddDays(7));
        }

        public string EncodeJwt(IPayload payload, DateTime exp)
        {
            SigningCredentials credentials = new SigningCredentials(_SecurityKey, SecurityAlgorithms.HmacSha512);

            JwtSecurityToken secToken = new JwtSecurityToken(
                _Issuer,
                _Audience,
                ToClaims(payload),
                DateTime.UtcNow,
                exp,
                credentials
            );
            return _Handler.WriteToken(secToken);
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidateAudience = false,
                ValidIssuer = _Issuer,
                RequireSignedTokens = true,
                IssuerSigningKey = _SecurityKey
            };
        }

        private IEnumerable<Claim> ToClaims(IPayload payload)
        {
            yield return new Claim(ClaimTypes.Email, payload.Email);
            yield return new Claim(ClaimTypes.Role, payload.RoleName);
            yield return new Claim(ClaimTypes.NameIdentifier, payload.Id.ToString());
            
            foreach (PropertyInfo prop in payload.GetType().GetProperties().Where(p => p.CanRead && p.GetValue(payload) != null && !typeof(IPayload).GetProperties().Select(x => x.Name).Contains(p.Name)))
            {
                yield return new Claim(prop.Name, JsonConvert.SerializeObject(prop.GetValue(payload)).Replace("\"", string.Empty));
            }
        }
    }
}
