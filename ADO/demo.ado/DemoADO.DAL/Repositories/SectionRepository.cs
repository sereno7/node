﻿using DemoADO.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace DemoADO.DAL.Repositories
{
    public class SectionRepository : BaseRepository<Section>
    {

        #region Ancienne Version
        //private string _connectionString;
        //private string _providerName;

        //public SectionRepository(string connectionString, string providerName)
        //{
        //    _connectionString = connectionString;
        //    _providerName = providerName;
        //}

        //public IEnumerable<Section> Get(int limit = 20, int offset = 0)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        IDbCommand cmd = CreateCommand(
        //            conn,
        //            "SELECT * FROM Section ORDER BY Id DESC OFFSET @p2 ROWS FETCH NEXT @p1 ROWS ONLY", 
        //            new Dictionary<string, object>
        //            {
        //                { "@p1", limit }, { "@p2", offset }
        //                // DELETE FROM Section WHERE Id = @p1
        //            }
        //        );
        //        IDataReader r = cmd.ExecuteReader();
        //        //List<Section> result = new List<Section>();
        //        while (r.Read())
        //        {
        //            Section s = new Section();
        //            s.Id = (int)r["Id"];
        //            s.Name = (string)r["Name"];
        //            yield return s;
        //            //result.Add(s);
        //        }
        //        //return result;
        //    }
        //}

        //public Section GetById(int id)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        IDbCommand cmd = CreateCommand(
        //            conn,
        //            "SELECT * FROM Section WHERE id = @p1",
        //            new Dictionary<string, object> { { "@p1", id } }
        //        );
        //        IDataReader reader = cmd.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            Section s = new Section();
        //            s.Id = (int)reader["Id"];
        //            s.Name = (string)reader["Name"];
        //            return s;
        //        }
        //        return null;
        //    }
        //}

        //public int Insert(Section s)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        string query = "INSERT INTO Section OUTPUT INSERTED.Id VALUES (@p1)";
        //        Dictionary<string, object> parameters = new Dictionary<string, object>
        //        {
        //            { "@p1", s.Name }
        //        };
        //        IDbCommand cmd = CreateCommand(conn, query, parameters);
        //        int id = (int)cmd.ExecuteScalar();
        //        return id;
        //    }
        //}

        //public bool Update(Section s)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        //créer la commande
        //        IDbCommand cmd = CreateCommand(
        //            conn,
        //            "UPDATE Section SET Name = @p1 WHERE id = @p2",
        //            new Dictionary<string, object>
        //            {
        //                { "@p1", s.Name },
        //                { "@p2", s.Id }
        //            }
        //        );
        //        // Executer la requête
        //        int nb = cmd.ExecuteNonQuery();
        //        return nb != 0;
        //    }
        //}

        //public bool Delete(int id)
        //{
        //    using (IDbConnection conn = GetConnection())
        //    {
        //        // ouvrir la connection
        //        conn.Open();
        //        // creer la commande + paramètres
        //        string query = "DELETE FROM Section WHERE Id = @p1";
        //        Dictionary<string, object> parameters =
        //            new Dictionary<string, object>()
        //            {
        //                { "@p1", id }
        //            };
        //        IDbCommand cmd = CreateCommand(conn, query, parameters);
        //        // Executer La commande
        //        int nbLines = cmd.ExecuteNonQuery();
        //        return nbLines != 0;
        //    }
        //}

        //private IDbConnection GetConnection()
        //{
        //    DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

        //    // crée ma connection
        //    IDbConnection connection = factory.CreateConnection();

        //    // set ma connection à ma connection
        //    connection.ConnectionString = _connectionString;

        //    return connection;
        //}

        //private IDbCommand CreateCommand(IDbConnection conn, string text, Dictionary<string, object> parameters = null)
        //{
        //    IDbCommand cmd = conn.CreateCommand();
        //    cmd.CommandText = text;
        //    if(parameters != null)
        //    {
        //        foreach (KeyValuePair<string, object> kvp in parameters)
        //        {
        //            IDataParameter p = cmd.CreateParameter();
        //            p.ParameterName = kvp.Key;
        //            p.Value = kvp.Value;
        //            cmd.Parameters.Add(p);
        //        }
        //    }
        //    return cmd;
        //} 
        #endregion
        public SectionRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
