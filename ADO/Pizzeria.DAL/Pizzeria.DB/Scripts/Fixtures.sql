﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO Category VALUES
('PIZZA'),
('BOISSON'),
('PATES');

INSERT INTO Product VALUES
('4 Fromages', 'Bleu, mozza, pecorino, parmesan', NULL, 15, 1),
('Margherita', 'Mozza, tomate', NULL, 13, 1),
('Prosciutto', 'jambon, Mozza, tomate', NULL, 14, 1),
('Carbonnara', 'Parmesan, oeuf', NULL, 12, 2),
('Coca', 'Boisson petillante', NULL, 2, 3);

INSERT INTO [User] VALUES
('LY', 'KHUN','KHUN@GMAIL.COM','Admin', HASHBYTES('SHA2_512', 'KHUN')),
('LY', 'PIV', 'PIV@GMAIL.COM', 'Admin', HASHBYTES('SHA2_512', 'PIV')),
('GIACOMELLI', 'SERENO', 'SERENO@GMAIL.COM','Client', HASHBYTES('SHA2_512', 'SERENO'));

