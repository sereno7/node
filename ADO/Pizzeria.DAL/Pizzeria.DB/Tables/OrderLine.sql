﻿CREATE TABLE [dbo].[OrderLine]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Quantity] NUMERIC NOT NULL, 
    [Price] MONEY NOT NULL, 
    [ID_Pro]INT NULL REFERENCES Product ON DELETE SET NULL,
    [ID_Ord]INT NULL REFERENCES [Order] ON DELETE CASCADE
)
