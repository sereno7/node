﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Filters;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        [Role("Customer", "Admin")]
        public ActionResult Index()
        {
            // Recuperer les catégories dans la db
            ProductRepository repo = ServicesLocator.GetService<ProductRepository>();
            IEnumerable<Product> produits = repo.Get();
            IEnumerable<ProductModel> model = produits.Select(x => x.Mapto<ProductModel>());
            return View(model);
        }

        [HttpGet]
        [Role("Admin")]
        public ActionResult Add()
        {
            ProductModel model = new ProductModel();
            ProductRepository repo = ServicesLocator.GetService<ProductRepository>();
           
            return View(model);
        }


        [HttpPost]
        [Role]
        public ActionResult Add(ProductModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.PostedFile != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    model.PostedFile.SaveAs(HostingEnvironment.MapPath("~/Content/Images/Products/") + fileName + model.PostedFile.FileName);
                    model.Image = "/Content/Images/Products/" + fileName + model.PostedFile.FileName;
                }
                ProductRepository repo = ServicesLocator.GetService<ProductRepository>();
                Product c = model.Mapto<Product>();
                repo.Insert(c);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [Role]
        public ActionResult Delete(int id)
        {
            ProductRepository repo = ServicesLocator.GetService<ProductRepository>();
            repo.Delete(id);
            return RedirectToAction("Index");
        }
        //--------------------------------------



        [HttpGet]
        [Role]
        public ActionResult Edit(int Id)
        {
            //Recuperer la connection à la table produit
            ProductModel model = new ProductModel();
            ProductRepository repo = ServicesLocator.GetService<ProductRepository>();
            //Recupérer le produit dont l'id est id
            Product p = repo.GetById(Id);
            //Fournir le produit mapper à la vue          
            if(p==null)
            {
                return new HttpNotFoundResult();
            }

            return View(p.Mapto<ProductModel>());
        }

        [HttpPost]
        [Role]
        public ActionResult Edit(ProductModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.PostedFile != null)
                {
                    if (model.Image != null)
                    {
                        System.IO.File.Delete(HostingEnvironment.MapPath(model.Image));
                    }

                    string fileName = Guid.NewGuid().ToString();
                    model.PostedFile.SaveAs(HostingEnvironment.MapPath("~/Content/Images/Products/") + fileName + model.PostedFile.FileName);
                    model.Image = "/Content/Images/Products/" + fileName + model.PostedFile.FileName;
                }
                ProductRepository repo = ServicesLocator.GetService<ProductRepository>();
                Product c = model.Mapto<Product>();
                repo.Update(c);
                return RedirectToAction("Index");
            }
            return View(model);
            
        }
    }
}


