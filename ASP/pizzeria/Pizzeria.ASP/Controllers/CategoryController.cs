﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            // Recuperer les catégories dans la db
            CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();
            IEnumerable<Category> categories = repo.Get();
            IEnumerable<CategoryModel> model = categories.Select(x => x.Mapto<CategoryModel>());
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(CategoryModel model)
        {
            if (ModelState.IsValid)
            {      
                CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();
                Category c = model.Mapto<Category>();
                repo.Insert(c);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();
            repo.Delete(id);
            return RedirectToAction("Index");           
        }
    }
}