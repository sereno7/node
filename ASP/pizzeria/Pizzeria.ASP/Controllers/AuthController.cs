﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using Pizzeria.ASP.Services;

namespace Pizzeria.ASP.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if(ModelState.IsValid)
            {
                UserRepository repo 
                    = ServicesLocator.GetService<UserRepository>();
                //mapping
                User u = model.Mapto<User>();
                //récupérer le pass pour le hasher
                string ToHash = model.PlainPassword;
                byte[] encoded
                    = ServicesLocator.GetService<HashService>()
                        .HashPassword(model.PlainPassword);
                u.Password = encoded;
                u.Role = "Customer";
                repo.Insert(u);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if(ModelState.IsValid)
            {
                UserRepository repo = ServicesLocator.GetService<UserRepository>();
                User u = repo.Get().SingleOrDefault(x => x.Email == model.Email);
                if(u == null)
                {
                    ViewBag.Error = "Bad Credentials";
                    return View();
                }            
                byte[] encoded
                    = ServicesLocator.GetService<HashService>()
                        .HashPassword(model.PlainPassword);

                //On ne peut pas comparer des tableaux de bytes,  on doit d'abord les reconvertir en chaine de char
                if (Encoding.UTF8.GetString(u.Password)
                    == Encoding.UTF8.GetString(encoded)
                )
                {
                    Session["Role"] = u.Role;
                    Session["Email"] = u.Email;
                    Session["Id"] = u.Id;
                    return RedirectToAction("index", "Product");
                }
            }
            return View(model);
        }

        
    }   
}