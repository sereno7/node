﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            // Recuperer les catégories dans la db
            OrderRepository repo = ServicesLocator.GetService<OrderRepository>();
            IEnumerable<Order> Commande = repo.Get();
            IEnumerable<OrderModel> model = Commande.Select(x =>
                new OrderModel
                {
                    Id = x.Id,
                    Reference = x.Reference,
                    Date = x.Date,
                    Status = x.Status,
                    UserId = x.UserId,
                });
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(OrderModel model)
        {
            if (ModelState.IsValid)
            {

                OrderRepository repo = new OrderRepository(
                    @"Server=5246\SQLSERVER;Initial Catalog=Pizzeria.DB;UID=sa;PWD=Test1234=",
                    "System.Data.SqlClient"
                    );
                Order o = new Order
                {
                    Id = model.Id,
                    Reference = model.Reference,
                    Date = model.Date,
                    Status = model.Status,
                    UserId = model.UserId,
                };

                repo.Insert(o);
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }



}