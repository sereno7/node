﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Validators
{
    public class NotDoubleInMyDBAttribute : ValidationAttribute
    {
        public NotDoubleInMyDBAttribute()
        {
            ErrorMessage = "Cette catégorie existe déjà";
        }

        public override bool IsValid(object value)
        {
            CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();
            IEnumerable<Category> liste = repo.Get();           
            foreach (var item in liste)
            {
                if (item.Name == value as string)
                    return false;
            }
            return true;
        }
    }
}