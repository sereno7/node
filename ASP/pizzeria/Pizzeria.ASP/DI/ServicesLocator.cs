﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Pizzeria.ASP.Services;
using Pizzeria.DAL.Repositories;

namespace Pizzeria.ASP.DI
{
    public static class ServicesLocator
    {
        private static ServiceCollection Services = new ServiceCollection();
        private static ServiceProvider Provider;
        static ServicesLocator()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider = ConfigurationManager.ConnectionStrings["default"].ProviderName;
            Services.AddTransient(
                (x) => new CategoryRepository(connectionstring,provider)
            );
            Services.AddTransient(
                (x) => new ProductRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new UserRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new OrderRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new OrderLineRepository(connectionstring, provider)
            );


            // Service de cryptage register
            // Meilleurs version
            Services.AddSingleton<HashService>();
            Services.AddSingleton<HashAlgorithm, SHA512CryptoServiceProvider>();
            
            // Service de cryptage
            // alternative moins propre
            //Services.AddSingleton<HashService>(
            //    (x) => new HashService(new SHA512CryptoServiceProvider())
            //    );

            Provider = Services.BuildServiceProvider();
        }
        public static T GetService<T>()
        {
            return Provider.GetService<T>();
        }

    }
}