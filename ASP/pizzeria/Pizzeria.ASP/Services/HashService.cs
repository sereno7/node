﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Pizzeria.ASP.Services
{
    public class HashService
    {
        private HashAlgorithm _algo;
        public HashService(HashAlgorithm algorithm)
        {
            _algo = algorithm;
        }  
        public byte[] HashPassword(string password)
        {
            return _algo.ComputeHash(Encoding.UTF8.GetBytes(password));
        }
     }
}