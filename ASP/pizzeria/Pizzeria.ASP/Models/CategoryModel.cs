﻿using Pizzeria.ASP.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Required]
        [NotDoubleInMyDBAttribute]
        public string Name { get; set; }
    }
}