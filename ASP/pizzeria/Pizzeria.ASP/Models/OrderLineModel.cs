﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class OrderLineModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
    }
}