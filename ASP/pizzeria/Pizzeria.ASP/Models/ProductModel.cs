﻿using Pizzeria.ASP.DI;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class ProductModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [MinLength(2)]
        public string Name { get; set; }

        [MaxLength(1000)]
        [Required]
        public string Description { get; set; }
        public string Image { get; set; }
        [Required]
        [Range(0,1000)]
        public decimal Price { get; set; }
        public int? CategoryId { get; set; }

        public HttpPostedFileBase PostedFile { get; set; }

        public IEnumerable<CategoryModel> ListCategory {get; set;}



        //Lazy loading
        private IEnumerable<CategoryModel> _allCategories;

        public IEnumerable<CategoryModel> allCategory
        {
            get
            {
                if (_allCategories != null)
                    return _allCategories;
                CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();
                IEnumerable<Category> categories = repo.Get();
                return _allCategories = categories.Select(x =>
                    new CategoryModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                    }); 
                
                
            }
        }
        
    }
}