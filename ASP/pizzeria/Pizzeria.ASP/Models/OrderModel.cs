﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string Reference { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public int? UserId { get; set; }

        public IEnumerable<OrderLineModel> ListOrderLine { get; set; }
    }
}