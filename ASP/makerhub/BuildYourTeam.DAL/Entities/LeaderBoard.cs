﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class LeaderBoard
    {
        public int Id { get; set; }
        public int Result { get; set; }
        public int IdActivity { get; set; }
        public int IdTeam { get; set; }
        public int IdDay { get; set; }
    }
}
