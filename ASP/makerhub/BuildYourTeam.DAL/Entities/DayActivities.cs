﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class DayActivities
    {
        public int Id { get; set; }
        public int IdDay { get; set; }
        public int IdActivity { get; set; }
    }
}
