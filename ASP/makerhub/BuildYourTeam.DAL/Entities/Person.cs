﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class Person
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? IdTeams { get; set; }
        public string Role { get; set; }
        [Required]
        public string PlainPassword { get; set; }
        [Required]
        public string Login { get; set; }
        public int? IdDay { get; set; }

    }
}
