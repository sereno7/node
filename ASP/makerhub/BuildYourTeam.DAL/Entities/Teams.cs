﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildYourTeam.DAL.Entities
{
    public class Teams
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }

        public string Slogan { get; set; }
        public int TotalPoint { get; set; }

        public int IdDay { get; set; }
    }
}
