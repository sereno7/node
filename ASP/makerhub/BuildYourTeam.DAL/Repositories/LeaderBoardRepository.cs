﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class LeaderBoardRepository : BaseRepository<LeaderBoard>
    {
        public LeaderBoardRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
