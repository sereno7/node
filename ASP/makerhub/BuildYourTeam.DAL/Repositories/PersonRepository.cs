﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class PersonRepository : BaseRepository<Person>
    {
        public PersonRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
        public Person Login(string login, string password)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                IDbCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Login";
                var params1 = cmd.CreateParameter();
                var params2 = cmd.CreateParameter();
                params1.ParameterName = "Login";
                params1.Value = login;
                params2.ParameterName = "PlainPassword";
                params2.Value = password;
                cmd.Parameters.Add(params1);
                cmd.Parameters.Add(params2);
                IDataReader r = cmd.ExecuteReader();
                Person result = null;
                if (r.Read())
                {
                    result = new Person
                    {
                        Id = (int)r["Id"],
                        Name = (string)r["Name"],
                        Login = (string)r["Login"],
                        IdTeams = r["IdTeams"] as int?,
                        IdDay = r["IdDay"] as int?,
                        Role = (string)r["Role"]
                    };
                }
                conn.Close();
                return result;
            }

            //public bool DeleteByTeamsId(int id)
            //{
            //    using (IDbConnection conn = GetConnection())
            //    {
            //        conn.Open();
            //        string query = "DELETE FROM [Person] WHERE [IdTeams] = @p1";
            //        Dictionary<string, object> parameters =
            //            new Dictionary<string, object>()
            //            {
            //                { "@p1", id }
            //            };
            //        IDbCommand cmd = CreateCommand(conn, query, parameters);
            //        int nbLines = cmd.ExecuteNonQuery();
            //        return nbLines != 0;
            //    }
            //}
        }
    }
}
