﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class DayActivitiesRepository : BaseRepository<DayActivities>
    {
        public DayActivitiesRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
              
        }
        public IEnumerable<DayActivities> GetByDayId(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM [DayActivities] WHERE [IdDay] = @p1";
                Dictionary<string, object> parameters =
                    new Dictionary<string, object>()
                {
                    { "@p1", id}
                };
                IDbCommand cmd = CreateCommand(conn, query, parameters);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }


        
        //    {
        //        conn.Open();
        //        string query = "DELETE FROM [Person] WHERE [IdTeams] = @p1";
        //        Dictionary<string, object> parameters =
        //            new Dictionary<string, object>()
        //            {
        //                { "@p1", id }
        //            };
        //        IDbCommand cmd = CreateCommand(conn, query, parameters);
        //        int nbLines = cmd.ExecuteNonQuery();
        //        return nbLines != 0;
        //    }
        //}
    }   

}
