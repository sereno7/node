﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace BuildYourTeam.DAL.Repositories
{
    public class TeamsRepository : BaseRepository<Teams>
    {
        public TeamsRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
