﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO [Role] VALUES
('Admin'),
('Player')

INSERT INTO [Person] VALUES (
	'a',
	'a',
    NULL,
	1,
	HASHBYTES('SHA2_512','a'),
	'Admin')

INSERT INTO [Activity] VALUES
('Activite1','test1'),
('Activite2','test2'),
('Activite3','test3'),
('Activite4','test4')
