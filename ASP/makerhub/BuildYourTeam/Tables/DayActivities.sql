﻿CREATE TABLE [dbo].DayActivities
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[IdDay] INT NOT NULL,
	[IdActivity] INT NOT NULL,
	CONSTRAINT FK_DAY FOREIGN KEY (IdDay) REFERENCES [Day] (Id) ON DELETE CASCADE,
	CONSTRAINT FK_Activity FOREIGN KEY (IdActivity) REFERENCES [Activity] (Id)

)
