﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Validator
{
    public class NotDoubleInMyDBAttribute : ValidationAttribute
    {
        public NotDoubleInMyDBAttribute()
        {
            ErrorMessage = "Cette catégorie existe déjà";
        }

        public override bool IsValid(object value)
        {
            RoleRepository repo = ServicesLocator.GetService<RoleRepository>();
            IEnumerable<Role> liste = repo.Get();
            foreach (var item in liste)
            {
                if (item.Name == value as string)
                    return false;
            }
            return true;
        }
    }
}