﻿using BuildYourTeam.ASP.Services;
using BuildYourTeam.DAL.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace BuildYourTeam.ASP.DI
{
    public static class ServicesLocator
    {
        private static ServiceCollection Services = new ServiceCollection();
        private static ServiceProvider Provider;
        static ServicesLocator()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider = ConfigurationManager.ConnectionStrings["default"].ProviderName;
            Services.AddTransient(
                (x) => new PersonRepository(connectionstring, provider)
            );
            Services.AddTransient(
                (x) => new LogRepository(connectionstring, provider)
            );
            //Services.AddTransient(
            //    (x) => new TeamsRepository(connectionstring, provider)
            //);
            //Services.AddTransient(
            //    (x) => new RoleRepository(connectionstring, provider)
            //);
            //Services.AddTransient(
            //    (x) => new DayRepository(connectionstring, provider)
            //);
            //Services.AddTransient(
            //    (x) => new ActivityRepository(connectionstring, provider)
            //);
            //Services.AddTransient(
            //    (x) => new LeaderBoardRepository(connectionstring, provider)
            //);
            //Services.AddTransient(
            //    (x) => new DayActivitiesRepository(connectionstring, provider)
            //);

            // Service de cryptage register
            //Services.AddSingleton<HashService>();
            //Services.AddSingleton<HashAlgorithm, SHA512CryptoServiceProvider>();

            Provider = Services.BuildServiceProvider();
        }
        public static T GetService<T>()
        {
            return Provider.GetService<T>();
        }

    }
}