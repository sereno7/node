﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.ASP.Services;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class AuthController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                PersonRepository repo = ServicesLocator.GetService<PersonRepository>();
                
                PersonModel u = repo.Login(model.Login, model.PlainPassword).Mapto<PersonModel>();
                if(u != null)
                {
                    return RedirectToAction("Index", "Log");
                }
                
                //Person u = repo.Get().SingleOrDefault(x => x.Login == model.Login);
                //if (u == null)
                //{
                //    ViewBag.Error = "Bad Credentials";
                //    return View();
                //}
                //byte[] encoded
                //    = ServicesLocator.GetService<HashService>()
                //        .HashPassword(model.PlainPassword);

                ////On ne peut pas comparer des tableaux de bytes,  on doit d'abord les reconvertir en chaine de char
                //if (Encoding.UTF8.GetString(u.Password)
                //    == Encoding.UTF8.GetString(encoded)
                //)
                //{
                //    Session["Role"] = u.IdRole;
                //    Session["Login"] = u.Login;
                //    Session["Id"] = u.Id;
                //    return RedirectToAction("Index", "Home");
                //}
            }
            return View(model);
        }
    }
}