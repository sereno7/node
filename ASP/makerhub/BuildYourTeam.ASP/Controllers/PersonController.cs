﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Filters;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        //[Role("Admin")]
        public ActionResult Index()
        {
            return View();
        }

        //[Role("Admin")]
        public ActionResult Delete(int id)
        {
            PersonRepository repo = ServicesLocator.GetService<PersonRepository>();
            repo.Delete(id);
            return RedirectToAction("Index", "Home");
        }

    }
}