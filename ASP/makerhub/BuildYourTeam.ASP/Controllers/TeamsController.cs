﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Filters;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class TeamsController : Controller
    {
        // GET: Teams
        //[Role("Admin")]
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        //[Role("Admin")]
        public ActionResult Edit(int Id)
        {
            TeamsModel model = new TeamsModel();
            TeamsRepository repo = ServicesLocator.GetService<TeamsRepository>();
            Teams t = repo.GetById(Id);
            //Fournir le produit mapper à la vue          
            if (t == null)
            {
                return new HttpNotFoundResult();
            }

            return View(t.Mapto<TeamsModel>());
        }

        [HttpPost]
        //[Role("Admin")]
        public ActionResult Edit(TeamsModel model)
        {
            if (ModelState.IsValid)
            {
                PersonRepository repo = ServicesLocator.GetService<PersonRepository>();               
                Person t = model.NewPlayer.Mapto<Person>();
                t.IdRole = 2;
                t.IdTeams = model.Id;
                t.Password = new byte[0];
                repo.Insert(t);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        //[Role("Admin")]
        public ActionResult Delete(int id)
        {
            //PersonRepository personrepo = ServicesLocator.GetService<PersonRepository>();

            //personrepo.DeleteByTeamsId(id);

            TeamsRepository teamsrepo = ServicesLocator.GetService<TeamsRepository>();

            teamsrepo.Delete(id);

            return RedirectToAction("Index", "Home");
        }
    }
}