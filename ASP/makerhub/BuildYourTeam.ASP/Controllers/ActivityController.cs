﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class ActivityController : Controller
    {

       
        [HttpGet]
        //[Role("Admin")]
        public ActionResult AddActivity()
        {
            IEnumerable<ActivityModel> model = ServicesLocator
                .GetService<ActivityRepository>().Get().Select(x => x.Mapto<ActivityModel>());

            return View(model);
            //return RedirectToAction("AddActivity", "Activity");
        }
        //[HttpPost]
        ////[Role("Admin")]
        //public ActionResult AddActivity(ActivityModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ActivityRepository repo = ServicesLocator.GetService<ActivityRepository>();
        //        Activity c = model.Mapto<Activity>();
        //        repo.Insert(c);
        //        return RedirectToAction("AddActivity", "Activity");
        //    }
        //    return View(model);
        //}
    }
}