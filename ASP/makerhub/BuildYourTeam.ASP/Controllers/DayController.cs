﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Filters;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.ASP.Models.DayControllerModel;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class DayController : Controller
    {
        // GET: Day
        [HttpGet]
        //[Role("Admin")]
        //public ActionResult Edit(int Id)
        //{
        //    //Recuperer la connection à la table produit
        //    DayModel model = new DayModel();
        //    DayRepository repo = ServicesLocator.GetService<DayRepository>();
        //    //Recupérer le produit dont l'id est id
        //    Day p = repo.GetById(Id);
        //    //Fournir le produit mapper à la vue          
        //    if (p == null)
        //    {
        //        return new HttpNotFoundResult();
        //    }

        //    return View(p.Mapto<DayModel>());
        //}
        public ActionResult Edit(int Id)
        {

            DayWactivityListModel model = new DayWactivityListModel();
            //Recuperer la connection à la table produit
            DayRepository repo = ServicesLocator.GetService<DayRepository>();
            //Recupérer le produit dont l'id est id
            Day p = repo.GetById(Id);
            model.Id = p.Id;
            model.CustomerName = p.CustomerName;
            model.Date = p.Date;
            DayActivitiesRepository repo2 = ServicesLocator.GetService<DayActivitiesRepository>();
            model.DayActivityList = repo2.GetByDayId(Id).Select(x => x.Mapto<DayActivitiesModel>());
            return View(model);
        }

        [HttpPost]
        //[Role("Admin")]
        public ActionResult Edit(DayModel model)
        {
            if (ModelState.IsValid)
            {
                TeamsRepository repo = ServicesLocator.GetService<TeamsRepository>();
                Teams c = model.NewTeam.Mapto<Teams>();
                c.IdDay = model.Id;
                repo.Insert(c);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }


        [HttpGet]
        //[Role("Admin")]
        public ActionResult AddEvent()
        {
            DayModel model = new DayModel();
            //DayWactivityListModel model = new DayWactivityListModel();
            return View(model);

            //ServicesLocator
            //.GetService<ActivityRepository>().Get().Select(x => x.Mapto<DayWactivityListModel>());
            //DayActivitiesModel dayActivityModel = new DayActivitiesModel();s
            //DayModel repo = ServicesLocator.GetService<DayModel>();
        }

        [HttpPost]
        //[Role("Admin")]
        public ActionResult AddEvent(DayModel model)
        {
            if (ModelState.IsValid)
            {
                DayRepository repo = ServicesLocator.GetService<DayRepository>();
                Day c = model.Mapto<Day>();
                repo.Insert(c);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
            //if (ModelState.IsValid)
            //{
            //    DayActivitiesRepository repo2 = ServicesLocator.GetService<DayActivitiesRepository>();

            //    DayRepository repo = ServicesLocator.GetService<DayRepository>();
            //    Day c = new Day()
            //    {
            //        CustomerName = model.CustomerName,
            //        Date = model.Date
            //    };
            //}
            //return View(model);
        }


        //[Role("Admin")]
        public ActionResult Delete(int id)
        {
            DayRepository repo = ServicesLocator.GetService<DayRepository>();
            repo.Delete(id);
            return RedirectToAction("Index", "Home");
        }

    }
}