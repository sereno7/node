﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class LogController : Controller
    {
        // GET: Log
        public ActionResult Index()
        {
            LogRepository repoLog = ServicesLocator.GetService<LogRepository>();
            IEnumerable<LogModel> l = repoLog.Get().Select(x => x.Mapto<LogModel>());
            return View(l);
        }
    }
}