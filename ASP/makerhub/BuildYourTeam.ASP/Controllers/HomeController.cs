﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Filters;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.ASP.Models;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuildYourTeam.ASP.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //[Role("Admin")]
        public ActionResult Index()
        {
            DayRepository repo = ServicesLocator.GetService<DayRepository>();
            IndexModel m = new IndexModel();
            IEnumerable<Day> listDay = repo.Get();
            IEnumerable<DayModel> model = listDay.Select(x => x.Mapto<DayModel>());
            //IEnumerable<DayModel> models = listDay.Select(x => new DayModel
            //{
            //    Id = x.Id,
            //    CustomerName = x.CustomerName,
            //    Date = x.Date
            //});

            return View(model);
        }

    }
}