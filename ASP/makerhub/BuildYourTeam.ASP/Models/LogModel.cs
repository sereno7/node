﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class LogModel
    {
        public int id { get; set; }
        public string Message { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public string Exception { get; set; }
    }
}