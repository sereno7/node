﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class IndexModel
    {
        public IEnumerable<DayModel> ListDay { get; set; }
        public IEnumerable<ActivityModel> ListActivity { get; set; }
    }
}