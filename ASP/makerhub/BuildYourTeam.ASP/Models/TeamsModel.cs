﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class TeamsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }

        public string Slogan { get; set; }
        public int? TotalPoint { get; set; }
        public int IdDay { get; set; }

        public PersonModel NewPlayer { get; set; } = new PersonModel();

        public IEnumerable<PersonModel> ListPlayers
        {
            get
            {
                PersonRepository repo = ServicesLocator.GetService<PersonRepository>();
                IEnumerable<Person> Player = repo.Get().Where((x) => x.IdTeams == Id);
                return Player.Select(x => x.Mapto<PersonModel>());
            }
        }
    }
}