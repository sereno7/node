﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class ActivityModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}