﻿using BuildYourTeam.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class PersonModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? IdTeams { get; set; }
        public string Role { get; set; }
        [Required]
        public string PlainPassword { get; set; }
        [Required]
        public string Login { get; set; }
        public int? IdDay { get; set; }
        //public bool IsAlreadyConnected {get; set;}


    }
}