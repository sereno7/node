﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class DayActivitiesModel
    {
        public int Id { get; set; }
        public int IdDay { get; set; }
        public int IdActivity { get; set; }

    }
}