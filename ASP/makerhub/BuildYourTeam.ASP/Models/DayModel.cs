﻿using BuildYourTeam.ASP.DI;
using BuildYourTeam.ASP.Mapper;
using BuildYourTeam.DAL.Entities;
using BuildYourTeam.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BuildYourTeam.ASP.Models
{
    public class DayModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public DateTime Date { get; set; }

        public TeamsModel NewTeam { get; set; } = new TeamsModel();

        public IEnumerable<TeamsModel> listTeams
        {
            get
            {
                TeamsRepository repo = ServicesLocator.GetService<TeamsRepository>();
                IEnumerable<Teams> Team = repo.Get().Where((x) => x.IdDay == Id);
                return Team.Select(x => x.Mapto<TeamsModel>());
            }
        }
        //public IEnumerable<Activity> listActivity
        //{
        //    get
        //    {
        //        ActivityRepository repo = ServicesLocator.GetService<ActivityRepository>();
        //        IEnumerable<Activity> Activity = repo.Get();
        //    }

        //}

    }
}