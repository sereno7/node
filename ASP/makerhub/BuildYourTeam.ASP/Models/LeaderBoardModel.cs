﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuildYourTeam.ASP.Models
{
    public class LeaderBoardModel
    {
        public int Id { get; set; }
        public int Result { get; set; }
        public int IdActivity { get; set; }
        public int IdTeam { get; set; }
        public int IdDay { get; set; }
    }
}