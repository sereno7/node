const {readFile} = require('fs');

const getDataFromJson = (filename) => {
    return new Promise((resolve, reject) => {
        readFile(filename, (error, data) => {
            if(error) {
                reject(error);
            }
            try {
                const json = JSON.parse(data);
                resolve(json);
            } catch (error) {
                reject(error)
            }
        })
    })
}

getDataFromJson('./datas/students.json').then(data => {
    for (const section of data.results) {
        console.log(`${results.section}`);
    }
}).catch(error => {
    console.log(`Une erreur c'est produite !`);
    console.log(error);
})