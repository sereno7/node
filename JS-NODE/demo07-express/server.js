const express = require('express');

//création serveur
const port = 8080;
const server = express();

//Definition des routes

server.get('/', (request, response) => {
    response.send('Hello World');
});

server.get('/test', (req, res) =>{
    res.writeHead(200, {'Content-type' : 'text/html'});
    res.end(`
        <h1>Page de Test</h1>
        <div>
            <p>technobel.be<p>
            <p>hello<p>
        </div>
    `);
})

server.get('/html', (req, res) => {
    const page = fs.readFileSync('./views/home/ejs', 'utf-8');
    const renduHtml = ejs.render(page);
    res.writeHead(200);
    res.end(renduHtml);
})

// Lancer le serveur

server.listen(port, () => {
    console.log(`Server is running on 127.0.0.1:${port}`)
})
