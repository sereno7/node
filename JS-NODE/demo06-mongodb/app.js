//importation du module Mangoose
const mongoose = require("mongoose");


//configuration pour MangoDB
const mongoDB = 'mongodb://localhost/Exemple';
mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology:true });

//Recuperer la connexion
const db = mongoose.connection;

// Gestion d'erreursur la db mongoDB
db.on('error', () =>{
    console.log("MonDB connection error");
});


// Definition d'un schéma mongoose
const personSchema = mongoose.Schema({
    firstname: String,
    lastname: String,
    age: Number
},{
    collection: 'Person',
});

const addPerson = (firstname, lastname, age) => {
    const person = mongoose.model("Person", personSchema);
    const newPerson = new person({
        firstname : firstname,
        lastname : lastname,
        age : age
    });
    newPerson.save((err, data) => {
        if(err){
            console.log("erreur lors de l'ajout!")
        }
        else{
            console.log("Personne ajouté.")
            console.log(data);
        }
    })
}

// addPerson('za', 'vanderquack', 13);
// addPerson('riri', 'duck', 14);
// addPerson('Donald', 'Duck', 30)
// addPerson('Daisy', 'Duck', 29)
// addPerson('Archibald', 'Gripsou', 70)
// addPerson('Batlthazar', 'Picsou', 67)

// -- Recuperation de tout les ducks

const getAllDuck = () =>{
    const Person = mongoose.model('Person', personSchema);
    Person.find({lastname: 'Duck'}, (err, data) => {
        if(err){
            console.log('Erreur lors de la recupétation');           
        }
        else{
            console.log(data);
        }
    })
}

getAllDuck()



